<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_phuongnam');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'L^R9#o3K3)id1eGtFbpPac(%sWkR@A/EPTaU0+)[pk(&O._<?WpM1`:~rD+LhJ|q');
define('SECURE_AUTH_KEY',  '?i@fgTM3e$mvK>1]>Y]wspJ%eN`>v%( |3:S>`9$6[Hf8afl@ND.c<WXNr|R<>Am');
define('LOGGED_IN_KEY',    '^e}6sb[,=]g3i!K;$g pve?rLx[:6oe18]{S}v4p[YYfUB^+KWZ,^y/1j=L{dTP3');
define('NONCE_KEY',        'A&4Qw48-!?9petsA/,w4Q]QV_-RnO2jS/f19=K}OY1nQKq.M3E`~~$5m0Ycn^u3r');
define('AUTH_SALT',        'uH7|`t(hU~?`mh&1Jw}SQcW2jjF$5@&?#(Vmin/~m!JAJ_gnt{CwxF+H+[Xc{I7A');
define('SECURE_AUTH_SALT', 'EY;dm#gw<7$$ZGArarC&t|YqoQZFe_~a<<U(7dTPa./kJ~t!#<f95!^$1fwYmoBx');
define('LOGGED_IN_SALT',   'e2ZdgPpb5cuY$D8i_lq-%fgi,G~A.>1eBP}/p+=%sgQF K]Z dHgn<V^(M6NLgVm');
define('NONCE_SALT',       '=gtM{Zmm}Dxoad!Z6hzajYRk30vbkBzg=,HK2nRJk$$[188g@I]5Kcl-z}}+e~mG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'pn_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
