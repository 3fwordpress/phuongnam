<?php

    if(isset($_POST['posts-taxonomies-options'])) {
        update_option('framework_remove-editor',$_POST['remove-editor']);
        update_option('framework_block-terms',$_POST['block-terms']);
        update_option('framework_block-posts',$_POST['block-posts']);
    }

    if(isset($_POST['other-options'])) {
        update_option('framework_remove-admin-menu',$_POST['remove-admin-menu-items']);
        update_option('framework_remove_plugin_row',$_POST['remove_plugin_row']);
        update_option('framework_replace_footer_text',$_POST['replace_footer_text']);
        update_option('framework_using-bootstrap-items',$_POST['using-bootstrap-items']);
        if($_POST['license_content'] != get_option('framework_license_content')) {
            WP_3FTheme::check_license($_POST['license_content']);
        }
        update_option('framework_license_content',$_POST['license_content']);
    }
    $allcats = get_categories(array('hide_empty'=>false));
    $all_post_types = get_post_types(array('show_ui'=> true, 'public' => true),'objects', 'and');
    $remove_plugin_row = get_option('framework_remove_plugin_row');
    $license = get_option('framework_license_content');
    $replace_footer_text = get_option('framework_replace_footer_text');
?>

<div class="wrap">
    <div class="contaier">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Post Type And Taxonomy Options</b></div>
                    <div class="panel-body">
                        <form method="post" action="">
                            <?php tw_block_posts_panel($all_post_types); ?>
                            <?php tw_block_terms_panel() ?>
                            <?php tw_remove_editor_panel($all_post_types); ?>
                            <input type="submit" name="posts-taxonomies-options" id="submit" class="btn btn-primary" value="Lưu thay đổi">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>More Options</b></div>
                    <div class="panel-body">
                        <form method="post" action="">
                            <div class="form-group">
                                <label for="license_content">Past Plugin License Lere: </label>
                                <input name="license_content" value="<?php echo $license ?>" id="license_content" class="form-control" placeholder="Enter license..."/>
                            </div>
                            <?php tw_remove_admin_menu_items() ?>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="remove_plugin_row"><input style="margin:0" type="checkbox" value="1" name="remove_plugin_row" <?php echo $remove_plugin_row?'checked':'' ?>  id="remove_plugin_row"/> Remove plugin row in table? </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="replace_footer_text"><input style="margin:0" type="checkbox" value="1" <?php echo $replace_footer_text?'checked':'' ?> name="replace_footer_text" id="replace_footer_text"/> Replace admin footer text? </label>
                                </div>
                            </div>
                            <input type="submit" name="other-options" id="submit" class="btn btn-primary" value="Lưu thay đổi">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(window).ready(function($){
        $('.select2-options').select2();
    });
</script>