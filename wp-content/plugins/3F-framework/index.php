<?php 
/**
* Plugin Name: 3F Framework
* Plugin URI: https://3fgrouop.vn
* Description: A plugin to help you create wordpress themes easier. Developed by Trieu Tai Niem
* Version: 1.2.9
* Author: Trieu Tai Niem
* Author URI: https://3fgroup.vn
* Text domain: twtheme
*/

if(!function_exists('add_action')) {
	echo "Hi there! We are 3F Group company! See us at https://3fgroup.vn ";
	exit();
}

session_start();

if(!defined('THEME_CORE_URL'))
    define( 'THEME_CORE_URL', plugin_dir_url( __FILE__ ));
    
//define the theme PATH constant, use it to include or requied the PHP files.
if(!defined('THEME_CORE_PATH'))
    define( 'THEME_CORE_PATH', plugin_dir_path( __FILE__ ));
    
define('TFT_URL', get_template_directory_uri());
define('TFT_PATH', get_template_directory());

define('CHECK_LICENSE_URL', 'https://3fgroup.vn/wp-json/licenses/check-license');
register_activation_hook( __FILE__, 'wp_3ftheme_core_active');
function wp_3ftheme_core_active() {
    $_SESSION['wp_theme_core_active'] = true;
}

if(isset($_SESSION['wp_theme_core_active'])) { 
    function wp_core_show_active_notice() { ?>
        <div class="notice notice-success is-dismissible">
            <p><b>3F Group:</b> 3F Framework đã được cài đặt, bây giờ bạn có thế sử dụng <b>Theme Template</b> do <b>3F Group Wordpress</b> cung cấp để tạo ra theme WP dễ dàng, nhanh chóng!</p>
        </div>
    <?php }
    add_action( 'admin_notices', 'wp_core_show_active_notice');
    unset($_SESSION['wp_theme_core_active']);
}

//include to WP_3FTheme, TF_Hooks
include 'classes/class.themecore.php';
include 'classes/class.hooks.php';
include 'plugins/plugin-init.php';