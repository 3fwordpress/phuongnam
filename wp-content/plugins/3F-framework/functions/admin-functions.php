<?php 

function tw_remove_editor_panel($all_post_types) {
  $editor_blocked = (array)get_option('framework_remove-editor'); ?>
  <label for="options-remove-editor">Remove Editor On:</label>
  <div class="form-group">
    <select id="options-remove-editor" class="form-control select2-options" name="remove-editor[]" multiple>
      <?php foreach($all_post_types as $type => $obj) {
          if(!post_type_supports($type, 'editor') || $type=='post') continue; ?>
          <optgroup label="<?php echo $obj->label ?>">
            <?php $allposts = get_posts(array('post_type'=>$type)); ?>
            <?php foreach($allposts as $p) { ?>
              <option <?php echo in_array($p->ID, $editor_blocked)?'selected':'' ?> value="<?php echo $p->ID ?>"><?php echo $p->post_title ?></option>
            <?php } ?>
          </optgroup>
      <?php } ?>
    </select>
  </div>
<?php }

function tw_block_posts_panel($all_post_types) {
  $posts_blocked = (array)get_option('framework_block-posts');?>
  <div class="form-group">
    <label for="options-block-posts">Block Posts:</label>
    <select id="options-block-posts" class="form-control select2-options" name="block-posts[]" multiple>
      <?php foreach($all_post_types as $type => $obj) { ?>
          <optgroup label="<?php echo $obj->label ?>">
            <?php $allposts = get_posts(array('post_type'=>$type)); ?>
            <?php foreach($allposts as $p) { ?>
              <option <?php echo in_array($p->ID, $posts_blocked)?'selected':'' ?> value="<?php echo $p->ID ?>"><?php echo $p->post_title ?></option>
            <?php } ?>
          </optgroup>
      <?php } ?>
    </select>
  </div>
<?php }

function tw_block_terms_panel() {
  $all_taxonomies = get_taxonomies(array('show_ui'=> true, 'public' => true),'objects', 'and');
  $terms_blocked = (array)get_option('framework_block-terms');?>
  <div class="form-group">
    <label for="options-block-terms">Block Terms:</label>
    <select id="options-block-terms" class="form-control select2-options" name="block-terms[]" multiple>
      <?php foreach($all_taxonomies as $tax => $obj) {
          if($tax=='post_tag') continue; ?>
          <optgroup label="<?php echo $obj->label ?>">
            <?php $allterms = get_terms(array('taxonomy'=>$tax, 'hide_empty' => 0)); ?>
            <?php foreach($allterms as $t) { ?>
              <option <?php echo in_array($t->term_id, $terms_blocked)?'selected':'' ?> value="<?php echo $t->term_id ?>"><?php echo $t->name ?></option>
            <?php } ?>
          </optgroup>
      <?php } ?>
    </select>
  </div>
<?php }

function tw_remove_admin_menu_items() {
  $menu = $GLOBALS['menu_backup'];
  $submenu = $GLOBALS['submenu_backup'];
  $removed = (array)get_option('framework_remove-admin-menu'); ?>
  <div class="form-group">
    <label for="remove-admin-menu-items">Remove Admin Menu Items:</label>
    <select id="remove-admin-menu-items" class="form-control select2-options" name="remove-admin-menu-items[]" multiple>
      <?php foreach($menu as $pr_key => $parent) {
          if(empty($parent[0])) continue;
          $remove_pr_key = 'pr>'.$pr_key; ?>
          <option <?php echo in_array($remove_pr_key,$removed)?'selected':'' ?> value="<?php echo $remove_pr_key ?>"><?php echo $parent[0] ?></option>
          <?php if(is_array($submenu[$parent[2]])) {
            foreach($submenu[$parent[2]] as $ch_key => $child) { 
              $remove_ch_key = 'ch>'.$pr_key.'>'.$ch_key; ?>
              <option <?php echo in_array($remove_ch_key, $removed)?'selected':'' ?> value="<?php echo $remove_ch_key ?>">&mdash; <?php echo $child[0] ?></option>
            <?php }
          } ?>
      <?php } ?>
    </select>
  </div>
<?php }