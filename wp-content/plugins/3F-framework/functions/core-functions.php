<?php

 /**
 * Theme core functions
 *
 * @since 1.0.0
 */


if( !function_exists( 'tw_category_dropdown' ) ):
    function tw_category_dropdown() {
        $tw_categories = get_categories( array( 'hide_empty' => 0 ) );
        $tw_category_dropdown['0'] = __( 'Chọn chuyên mục', 'twtheme' );
        foreach ( $tw_categories as $tw_category ) {
            $tw_category_dropdown[$tw_category->term_id] = $tw_category->cat_name;
        }
        return $tw_category_dropdown;
    }
endif;

/**
 * Define pages for dropdown
 *
 * @return array();
 */

if( !function_exists( 'tw_pages_dropdown' ) ):
    function tw_pages_dropdown() {
        $tw_pages = get_pages( array( 'hide_empty' => 0 ) );
        $tw_pages_dropdown['0'] = __( 'Chọn một trang', 'twtheme');
        foreach ( $tw_pages as $tw_page ) {
            $tw_pages_dropdown[$tw_page->ID] = $tw_page->post_title;
        }
        return $tw_pages_dropdown;
    }
endif;


if( !function_exists( 'tw_term_dropdown' ) ):
    function tw_term_dropdown($term) {
        $tw_terms = get_terms( array( 'hide_empty' => false , 'taxonomy' => $term) );
        $tw_terms_dropdown['0'] = __( 'Mời chọn', 'twtheme');
        foreach ( $tw_terms as $tw_term ) {
            $tw_terms_dropdown[$tw_term->term_id] = $tw_term->name;
        }
        return $tw_terms_dropdown;
    }
endif;

if( !function_exists( 'tw_sanitize_text_field' ) ):
    function tw_sanitize_text_field( $input ) {
        return $input;
    }
endif;

/**
 * Sanitize number
 */
if( !function_exists( 'tw_sanitize_number' ) ):
    function tw_sanitize_number( $input ) {
        $output = intval($input);
        return $output;
    }
endif;

// the function to check plugin is active
if(!function_exists('tw_is_plugin_active')) {
    function tw_is_plugin_active($plugin) {
        include_once(ABSPATH.'wp-admin/includes/plugin.php');
        return is_plugin_active($plugin);
    } 
}

// the function to check a folder is exitst
if(!function_exists('tw_folder_exists')) {
    function tw_folder_exists($path) {
        if(!file_exists($path)) {
            echo ('<b>ERROR:</b> Thư mục <b>'.$path.'</b> không tồn tại, vui lòng tạo thư mục trước khi sử dụng.');
        }
    } 
}

// the function to check a file is exitst
if(!function_exists('tw_file_exists')) {
    function tw_file_exists($path) {
        if(!file_exists($path)) {
            echo ('<b>ERROR:</b> File <b>'.$path.'</b> không tồn tại, vui lòng tạo file này trước khi sử dụng.');
        }
    } 
}


if(!function_exists('get_logo')) {
    function get_logo() {
        $custom_logo_id = get_field( 'custom_site_logo', 'option');
        $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
        return $logo[0];
    } 
}

function acf_get_google_map($location) { ?>
    <?php if( !empty($location) ):?>
        <style type="text/css">.acf-map {width: 100%;height: 400px;border: #ccc solid 1px;margin: 20px 0;}.acf-map img {max-width: inherit !important;}</style>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo acf_get_setting('google_api_key') ?>"></script>
        <div class="acf-map">
            <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
        </div>
        <?php
            add_action('wp_footer', function(){ ?>
                <script type="text/javascript">
                    (function($) {
                        function new_map( $el ) {
                            var $markers = $el.find('.marker');
                            var args = {
                                zoom		: 16,
                                center		: new google.maps.LatLng(0, 0),
                                mapTypeId	: google.maps.MapTypeId.ROADMAP
                            };        	
                            var map = new google.maps.Map( $el[0], args);
                            map.markers = [];
                            $markers.each(function(){
                                add_marker( $(this), map );
                            });
                            center_map( map );
                            return map;
                        }
        
                        function add_marker( $marker, map ) {
                            var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
                            var marker = new google.maps.Marker({
                                position	: latlng,
                                map			: map
                            });
                            map.markers.push( marker );
                            if( $marker.html() ) {
                                var infowindow = new google.maps.InfoWindow({
                                    content		: $marker.html()
                                });
                                google.maps.event.addListener(marker, 'click', function() {
                                    infowindow.open( map, marker );
                                });
                            }
                        }
                        
                        function center_map( map ) {
                            var bounds = new google.maps.LatLngBounds();
                            $.each( map.markers, function( i, marker ){
                                var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
                                bounds.extend( latlng );
                            });
                            if( map.markers.length == 1 ) {
                                map.setCenter( bounds.getCenter() );
                                map.setZoom( 16 );
                            } else {
                                map.fitBounds( bounds );
                            }
                        }
        
                        var map = null;
                        $(document).ready(function(){
                            $('.acf-map').each(function(){
                                map = new_map( $(this) );
                            });
                        });
                    })(jQuery);
                </script>
        <?php });
    endif;
}

function get_file_headers($file) {
    $hand = fopen( $file, 'r');
    $content = str_replace(array('<?php', '/**'), array('',''), fread($hand,8192));
    $content = explode('*/',$content);
    $content = trim($content[0]);
    $content = explode('* ',$content);
    unset($content[0]);
    $match = preg_grep ('/^Title/', $content);

    if(count($match) < 1) return;

    $page_info = null;
    foreach($content as $key => $line) {
      $h = explode(':', $line);
      if(count($h)<2) continue;
      $page_info[strtolower($h[0])] = trim($h[1]);
    }
    $page_info = wp_parse_args($page_info, array(
        'title' => '',
        'name' => '',
        'icon' => '',
        'slug' => str_replace('.php', '', basename($file)),
        'order' =>  50,
        'parent' => false,
        'capability' => 'manage_options'
    ));
    return $page_info;
}


function tw_send_mail(string $body,array $args, string $to, string $subject) {
    $replace = null;
    $values = null;
    foreach($args as $key => $value) {
        $replace[] = $key;
        $values[] = $value;
    }
    $body = str_replace($replace,$values,$body);
    $body = apply_filters('the_content', $body);
    $headers = array('Content-Type: text/html; charset=UTF-8');
    wp_mail( $to, $subject, $body, $headers );
}

function tw_set_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','tw_set_content_type' );