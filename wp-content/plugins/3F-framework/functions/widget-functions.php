<?php 
/**
* @author: Trieu Tai Niem
* @package: 3F Web
* 
*/

if(!function_exists('add_action')) {
	echo "Hi there! We are 3F Group company! See us at https://3fgroup.vn ";
	exit();
}

if( !function_exists( 'tw_widgets_show_widget_field' ) ):
function tw_widgets_show_widget_field( $instance = '', $widget_field = '', $tw_widget_field_value = '' ) {
    
    extract( $widget_field );

    switch ( $tw_widgets_field_type ) {

        // Standard text field
        case 'text': ?>
            <p>
                <label for="<?php echo esc_attr( $instance->get_field_id( $tw_widgets_name ) ); ?>"><?php echo esc_html( $tw_widgets_title ); ?>:</label>
                <input class="widefat" id="<?php echo esc_attr( $instance->get_field_id( $tw_widgets_name ) ); ?>" name="<?php echo esc_attr( $instance->get_field_name( $tw_widgets_name ) ); ?>" type="text" value="<?php echo esc_html( $tw_widget_field_value ); ?>" />

                <?php if ( isset( $tw_widgets_description ) ) { ?>
                    <br />
                    <small><em><?php echo esc_html( $tw_widgets_description ); ?></em></small>
                <?php } ?>
            </p>
        <?php break;

        // Textarea field
        case 'textarea': ?>
            <p>
                <label for="<?php echo esc_attr( $instance->get_field_id( $tw_widgets_name ) ); ?>"><?php echo esc_html( $tw_widgets_title ); ?>:</label>
                <textarea class="widefat" rows="<?php echo intval( $tw_widgets_row ); ?>" id="<?php echo esc_attr( $instance->get_field_id( $tw_widgets_name ) ); ?>" name="<?php echo esc_attr( $instance->get_field_name( $tw_widgets_name ) ); ?>"><?php echo esc_html( $tw_widget_field_value ); ?></textarea>
            </p>
        <?php break;

        // Select field
        case 'select' :
            if( empty( $tw_widget_field_value ) ) {
                $tw_widget_field_value = $tw_widgets_default;
            }?>

            <p>
                <label for="<?php echo esc_attr( $instance->get_field_id( $tw_widgets_name ) ); ?>"><?php echo esc_html( $tw_widgets_title ); ?>:</label>
                <select name="<?php echo esc_attr( $instance->get_field_name( $tw_widgets_name ) ); ?>" id="<?php echo esc_attr( $instance->get_field_id( $tw_widgets_name ) ); ?>" class="widefat">
                    <?php foreach ( $tw_widgets_field_options as $tw_option_name => $tw_option_title ) { ?>
                        <option value="<?php echo esc_attr( $tw_option_name ); ?>" id="<?php echo esc_attr( $instance->get_field_id( $tw_option_name ) ); ?>" <?php selected( $tw_option_name, $tw_widget_field_value ); ?>><?php echo esc_html( $tw_option_title ); ?></option>
                    <?php } ?>
                </select>

                <?php if ( isset( $tw_widgets_description ) ) { ?>
                    <br /><small><?php echo esc_html( $tw_widgets_description ); ?></small>
                <?php } ?>
            </p>
        <?php break;

        // Select field
        case 'multiselects' :
            if( empty( $tw_widget_field_value ) ) {
                $tw_widget_field_value = $tw_widgets_default;
            }?>

            <p>
                <label for="<?php echo esc_attr( $instance->get_field_id( $tw_widgets_name ) ); ?>"><?php echo esc_html( $tw_widgets_title ); ?>:</label>
                <select name="<?php echo esc_attr( $instance->get_field_name( $tw_widgets_name ) ); ?>[]" id="<?php echo esc_attr( $instance->get_field_id( $tw_widgets_name ) ); ?>" class="widefat" multiple>
                    <?php foreach ( $tw_widgets_field_options as $tw_option_name => $tw_option_title ) { ?>
                        <option value="<?php echo esc_attr( $tw_option_name ); ?>" id="<?php echo esc_attr( $instance->get_field_id( $tw_option_name ) ); ?>" <?php if(in_array($tw_option_name,(array)$tw_widget_field_value)) echo 'selected' ?>><?php echo esc_html( $tw_option_title ); ?></option>
                    <?php } ?>
                </select>

                <?php if ( isset( $tw_widgets_description ) ) { ?>
                    <br /><small><?php echo esc_html( $tw_widgets_description ); ?></small>
                <?php } ?>
            </p>
        <?php break;

        case 'number' :
            if( empty( $tw_widget_field_value ) ) {
                $tw_widget_field_value = $tw_widgets_default;
            } ?>
            <p>
                <label for="<?php echo esc_attr( $instance->get_field_id( $tw_widgets_name ) ); ?>"><?php echo esc_html( $tw_widgets_title ); ?>:</label>
                <input name="<?php echo esc_attr( $instance->get_field_name( $tw_widgets_name ) ); ?>" type="number" step="1" min="1" id="<?php echo esc_attr( $instance->get_field_id( $tw_widgets_name ) ); ?>" value="<?php echo esc_html( $tw_widget_field_value ); ?>" class="small-text" />

                <?php if ( isset( $tw_widgets_description ) ) { ?>
                    <br />
                    <small><?php echo esc_html( $tw_widgets_description ); ?></small>
                <?php } ?>
            </p>
        <?php break;

        case 'upload':
            $image = $image_class = "";
            if( $tw_widget_field_value ){ 
                $image = '<img src="'.wp_get_attachment_url($tw_widget_field_value ).'" style="max-width:100%;"/>';
                $image_class = ' hidden';
            } ?>

            <label for="<?php echo esc_attr( $instance->get_field_id( $tw_widgets_name ) ); ?>"><?php echo esc_html( $tw_widgets_title ); ?>:</label><br />
            <div class="attachment-media-view">
            
                <div class="placeholder<?php echo esc_attr( $image_class ); ?>">
                    <?php esc_html_e( 'Chưa có ảnh nào được chọn', 'tw' ); ?>
                </div>
                <div class="thumbnail thumbnail-image">
                    <?php echo $image; ?>
                </div>

                <div class="actions clearfix">
                    <button type="button" class="button mt-delete-button align-left"><?php _e( 'Xóa', 'tw' ); ?></button>
                    <button type="button" class="button mt-upload-button alignright"><?php _e( 'Chọn Ảnh', 'tw' ); ?></button>
                    
                    <input name="<?php echo esc_attr( $instance->get_field_name( $tw_widgets_name ) ); ?>" id="<?php echo esc_attr( $instance->get_field_id( $tw_widgets_name ) ); ?>" class="upload-id" type="hidden" value="<?php echo esc_url( $tw_widget_field_value ) ?>"/>
                </div>

                <?php if ( isset( $tw_widgets_description ) ) { ?>
                    <br/><small><?php echo wp_kses_post( $tw_widgets_description ); ?></small>
                <?php } ?>

            </div>

            <?php break;

        //Multi checkboxes
        case 'multicheckboxes': ?>
            <label><?php echo esc_html( $tw_widgets_title ); ?>:</label>

            <?php foreach ( $tw_widgets_field_options as $tw_option_name => $tw_option_title) {
                    if( isset( $tw_widget_field_value[$tw_option_name] ) ) {
                        $tw_widget_field_value[$tw_option_name] = 1;
                    } else{
                        $tw_widget_field_value[$tw_option_name] = 0;
                    } ?>

                    <div class="mt-single-checkbox">
                        <p>
                            <input id="<?php echo esc_attr( $instance->get_field_id( $tw_option_name ) ); ?>" name="<?php echo esc_attr( $instance->get_field_name( $tw_widgets_name ).'['.$tw_option_name.']' ); ?>" type="checkbox" value="1" <?php checked('1', $tw_widget_field_value[$tw_option_name]); ?>/>
                            <label for="<?php echo esc_attr( $instance->get_field_id( $tw_option_name ) ); ?>"><?php echo esc_html( $tw_option_title ); ?></label>
                        </p>
                    </div>
                <?php }

            if ( isset( $tw_widgets_description ) ) { ?>
                    <small><em><?php echo esc_html( $tw_widgets_description ); ?></em></small>
            <?php }

            break;

        case 'checkbox': ?>
            <?php if( isset( $tw_widget_field_value)) {
                    $tw_widget_field_value = 1;
                } else{
                    $tw_widget_field_value = 0;
                } ?>

                <div class="mt-single-checkbox">
                    <p>
                        <input id="<?php echo esc_attr( $instance->get_field_id( $tw_option_name ) ); ?>" name="<?php echo esc_attr( $instance->get_field_name( $tw_widgets_name )); ?>" type="checkbox" value="1" <?php checked('1', $tw_widget_field_value); ?>/>
                        <label for="<?php echo esc_attr( $instance->get_field_id( $tw_option_name ) ); ?>"><?php echo esc_html( $tw_widgets_title ); ?></label>
                    </p>
                </div>
            <?php if ( isset( $tw_widgets_description ) ) { ?>
                    <small><em><?php echo esc_html( $tw_widgets_description ); ?></em></small>
            <?php }

            break;
    }
}
endif;



if( !function_exists( 'tw_widgets_updated_field_value' ) ):
    function tw_widgets_updated_field_value( $widget_field, $new_field_value ) {

        extract( $widget_field );

        if ( $tw_widgets_field_type == 'number') {
            return tw_sanitize_number( $new_field_value );
        } elseif ( $tw_widgets_field_type == 'textarea' ) {
            return wp_kses_post( $new_field_value );
        } elseif ( $tw_widgets_field_type == 'url' ) {
            return esc_url( $new_field_value );
        } elseif( $tw_widgets_field_type == 'multicheckboxes' ) {
            return $new_field_value;
        } elseif( $tw_widgets_field_type == 'multiselects' ) {
            return $new_field_value;
        } else {
            return sanitize_text_field( $new_field_value );
        }
    }
endif;

if( !function_exists( 'tw_pages_dropdown' ) ):
    function tw_pages_dropdown() {
        $tw_pages = get_pages( array( 'hide_empty' => 0 ) );
        $tw_pages_dropdown['0'] = __( 'Chọn một trang', 'twtheme');
        foreach ( $tw_pages as $tw_page ) {
            $tw_pages_dropdown[$tw_page->ID] = $tw_page->post_title;
        }
        return $tw_pages_dropdown;
    }
endif;


//js and css for widgets
add_action( 'admin_enqueue_scripts',function( $hook ) {

    if( 'widgets.php' != $hook) {
        return;
    }

    if ( function_exists( 'wp_enqueue_media' ) ) {
        wp_enqueue_media();
    }

    wp_enqueue_script( 'jquery-ui-button' );

    wp_enqueue_script( 'tw-admin-script', THEME_CORE_URL .'/assets/scripts/widget-script.js', array('jquery'), '1.0.0', true );

    wp_enqueue_style( 'tw-admin-style', THEME_CORE_URL .'/assets/css/widget-style.css', '1.0.0' );

});