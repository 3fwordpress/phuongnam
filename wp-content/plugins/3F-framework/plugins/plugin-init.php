<?php

require_once 'class.plugin-activation.php';
add_action( 'tw_register', 'tw_register_required_plugins' );
function tw_register_required_plugins(){

	$plugins = array(
		array(
			'name'               => 'Advanced Custom Fields',
			'slug'               => 'advanced-custom-fields-pro',
			'source'             => THEME_CORE_PATH . '/plugins/advanced-custom-fields-pro.zip',
			'required'           => false,
			'version'            => 5.6,
			'force_activation'   => false,
			'force_deactivation' => true
		),
		array(
			'name'               => 'Easy WP SMTP',
			'slug'               => 'easy-wp-smtp',
			'source'             => THEME_CORE_PATH . '/plugins/easy-wp-smtp.zip',
			'required'           => false,
			'version'            => 1.3,
			'force_activation'   => false,
			'force_deactivation' => true
		)
	);

	$config = array(
		'id'           => 'twtheme',                		// Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      		// Default absolute path to bundled plugins.
		'menu'         => 'twtheme-install-plugin', 		// Menu slug.
		'parent_slug'  => 'wp-theme-options',        // Parent menu slug.
		'capability'   => 'manage_options',    			// Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    		// Show admin notices or not.
		'dismissable'  => false,                    		// If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      		// If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   		// Automatically activate plugins after installation or not.
		'message'      => '',                      		// Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );
}
