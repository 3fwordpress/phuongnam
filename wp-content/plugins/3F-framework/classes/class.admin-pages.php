<?php 
/**
* This file called by TFThemeCore::wppages_rigister()
* The class make a wordpress admin pages easier
* 
*/


class WP_Pages {

    //page property
    public $args;
    public $pages_folder = TFT_PATH.'/components/admin-pages/';

    //initialization page
    public function __construct($page_args) {
        $this->args = $page_args;
        add_action( 'admin_menu',function () {
            extract($this->args);
            add_menu_page(
                $title,
                $name, 
                $capability, 
                $slug,
                array(&$this, 'load_parent'),   
                $icon,
                $order
            );
        });
        $GLOBALS['bootstrap_page'][] = $this->args['slug'];
    }

    //add subpage to the menu
    public function add_subpage($args) {
        add_action( 'admin_menu',function() use ($args) {   
            extract($args);
            $parent_slug = $this->args['slug'];
            add_submenu_page(
                $parent_slug,
                $title,
                $name,
                $capability,
                $parent_slug.'_'.$slug,
                array('WP_Pages','tw_load_page')
            );
        });
        $GLOBALS['bootstrap_page'][] = $args['parent_slug'].'_'.$args['slug'];
    }

    //load content page from file
    public function load_parent() { ?>
         <div class="wrap">
            <h1 style="margin-bottom: 20px"><?php echo get_admin_page_title() ?></h1>
            <?php 
                tw_folder_exists($this->pages_folder);
                $path = $this->pages_folder.$this->args['slug'].'.php';
                tw_file_exists($path);
                require $path;
            ?>
        </div>
    <?php }

    //add subpage to the menu
    public static function add_submenu($args) {
        add_action( 'admin_menu',function() use ($args) {   
            extract($args);
            add_submenu_page(
                $parent,
                $title,
                $name,
                $capability, 
                $slug,
                array('WP_Pages','tw_load_page')
            );
        });

        $GLOBALS['bootstrap_page'][] = $args['slug'];
    }

    public static function tw_load_page() {
        global $pagenow; ?>
        <div class="wrap">
            <h1 style="margin-bottom: 20px"><?php echo get_admin_page_title() ?></h1>
            <?php 
                $pages_folder = TFT_PATH.'/components/admin-pages/';
                tw_folder_exists($pages_folder);
                tw_file_exists($pages_folder.$_GET['page'].'.php');
                require $pages_folder.$_GET['page'].'.php';
            ?>
        </div>
    <?php }
}

//add framework options page
add_action( 'admin_menu',function(){
    add_submenu_page(
        null,
        'Tùy chọn Plugin 3F Framework',
        '3F Framework Options',
        'manage_options', 
        'framework-options',
        'tw_framework_options'
    );
});

function tw_framework_options() { ?>
    <div class="wrap">
        <h1 style="margin-bottom: 20px">Framework Options</h1>
        <?php require THEME_CORE_PATH.'/views/framework-options.php'; ?>
    </div>
<?php }