<?php

class TF_Hooks {
    private $scr_allow_bootstap;
    function __construct() {
        add_filter( 'show_admin_bar', '__return_false');
        add_filter( 'feed_links_show_comments_feed', '__return_false' );

        //varible for ajax
        add_action( 'init', array($this,'disable_emojis' ));
        add_filter( 'nav_menu_link_attributes', array($this,'wpd_nav_menu_link_atts'), 20, 4 );
        add_filter( 'wp_mail_content_type',array($this,'set_content_type') );
        //change admin text

        $this->disable_emojis();
 
        //in admin
        if(is_admin()) {
            $GLOBALS['bootstrap_page'][] = 'framework-options';
            $this->remove_metabox();
            $this->add_scripts_in_admin();
            add_filter('contextual_help', array($this,'remove_help_tabs'), 999, 3 );
            add_action('admin_bar_menu', array($this,'remove_wp_logo'), 999 );
            add_action('admin_menu', array($this, 'remove_admin_menu_itemts'), 9999);
            add_filter('admin_footer_text', array($this,'change_footer_admin'), 9999);
            add_action( 'admin_head', array($this,'hide_update_notice_to_all_but_admin_users'), 1);
        }

        add_filter('login_headerurl', array($this,'login_logo_url' ));
        add_action('login_head', array($this,'change_login_logo' ));
    }

    function hide_update_notice_to_all_but_admin_users() {
        if (!current_user_can('update_core')) {
            remove_action( 'admin_notices', 'update_nag', 3 );
        }
    }

    function change_login_logo() { ?>
        <style type="text/css">
            h1 a {
                height: 0 !important; 
            }
            body {
                background-size: cover;
                background-image:url(<?php echo THEME_CORE_URL ?>/assets/images/login-bg.jpg) !important;
            }
            .login #backtoblog a, .login #nav a {
                color: #000000;
                font-weight: bold;
            }
            .login form {
                border-radius: 10px 10px 15px 15px;
            }
            </style>
    <?php }
        
    function login_logo_url() {
        return 'https://3fgroup.vn';
    }

    function add_scripts_in_admin() {
        $bootstrap_page = isset($GLOBALS['bootstrap_page'])?$GLOBALS['bootstrap_page']:array();

        if(isset($_GET['page']) && in_array($_GET['page'],$bootstrap_page)) {
            add_action('admin_head', function() {
                wp_enqueue_script('jquery');
                wp_enqueue_style('bootstrap-css', THEME_CORE_URL.'/assets/libraries/bootstrap/css/bootstrap.min.css');
                wp_enqueue_style('select2-css', THEME_CORE_URL.'/assets/libraries/select2/css/select2.min.css');
            });

            add_action('admin_footer', function() {
                wp_enqueue_script('bootstrap-js', THEME_CORE_URL.'/assets/libraries/bootstrap/js/bootstrap.min.js');
                wp_enqueue_script('select2-js', THEME_CORE_URL.'/assets/libraries/select2/js/select2.min.js');
            });
        }

        add_action('admin_footer', function() {
            if(get_option('framework_remove_plugin_row'))
                echo '<script>jQuery("tr[data-slug=3f-framework]").remove();
                var plCount = jQuery(".subsubsub .all .count");
            </script>';
        });
    }

    function remove_admin_menu_itemts() {
        global $menu, $submenu;
        $GLOBALS['menu_backup'] = $menu;
        $GLOBALS['submenu_backup'] = $submenu;
        $removed = (array)get_option('framework_remove-admin-menu');
        foreach($removed as $r) {
            $arr = explode('>',$r);
            if(count($arr) < 3) {
                unset($menu[$arr[1]]);
            } else {
                $parent = $menu[$arr[1]][2];
                unset($submenu[$parent][$arr[2]]);
            }
        }
    }

    function remove_wp_logo( $wp_admin_bar ) {
        $wp_admin_bar->remove_node( 'wp-logo' );
    }

    function remove_help_tabs( $old_help, $screen_id, $screen ){
        $screen->remove_help_tabs();
        return $old_help;
    }

    function remove_metabox() {
        add_action('wp_dashboard_setup', function() {
            global $wp_meta_boxes;
            unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
            unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
            unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
            unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
        });
    }

    function disable_emojis() {
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
        remove_action( 'admin_print_styles', 'print_emoji_styles' );    
        remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
        remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );      
        remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
        add_filter( 'tiny_mce_plugins', array($this,'disable_emojis_tinymce' ));
    }

    function disable_emojis_tinymce( $plugins ) {
        if ( is_array( $plugins ) ) {
                return array_diff( $plugins, array( 'wpemoji' ) );
        } else {
                return array();
        }
    }

    function change_footer_admin() {
        if(get_option('framework_replace_footer_text'))
            return __('Cảm ơn bạn đã sử dụng dịch vụ thiết kế website của <a href="https://3fgroup.vn">3F Group</a>', 'twtheme');        
        else 
            return __('Thank you for using Wordpress', 'wp');
    }
    
    function wpd_nav_menu_link_atts( $atts, $item, $args, $depth ){
        $atts['href'] = str_replace(array('http://%home%', 'http://%home%'), array(home_url(),home_url()), $atts['href']);
        return $atts;
    }

    function set_content_type(){
        return "text/html";
    }
}
new TF_Hooks();
