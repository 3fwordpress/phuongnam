<?php 
global $current_user;
if (isset($_POST)) {
	if (isset($_POST['submit_singin'])) {
		$args['user_login'] = $_POST['email'];
		$args['user_password'] = $_POST['password'];
		$args['remember'] = $_POST['remember']=='on'?true:false;
		$login = triip_login($args);
		// echo '<pre>'.__FILE__.'::'.__METHOD__.'('.__LINE__.')<br>'; 
		// 	print_r($login); 
		// echo '</pre>';
		?><?php
		if (is_wp_error($login)){ ?>
			<script type="text/javascript">
				document.getElementsByName('req_pass').innerHTML = '<?php echo $login->get_error_message(); ?>';
				// $('.req_pass').html('<?php //echo $login->get_error_message(); ?>');
			</script>
		<?php }
		else{ ?>
			<script type="text/javascript">
				alert("Bạn đã đăng nhập thành công!");
				//window.location = window.location;
			</script>
		<?php } ?>
		<?php
	}elseif(isset($_POST['submit_register'])) {
		$namex['first_name'] = strip_tags($_POST['firstname']);
		$namex['last_name'] = strip_tags($_POST['lastname']);
		$array_register['name'] = $namex['first_name'].' '.$namex['last_name'];
		$array_register['email'] = $_POST['email'];
		$array_register['password'] = $_POST['password'];
		$array_register['input-radio'] = $_POST['input-radio'];
		$mess= register_user($array_register,$namex);
		$_user = get_user_by( 'email', $_POST['email'] );
		$args['user_login'] =$_user->data->user_login;
		$args['user_password'] = $_POST['password'];
		$args['remember'] = false;

		// $log_in = is_user_logged_in();
		?>
		<script type="text/javascript">
			<?php if ($mess==1){ ?>
				alert("Bạn đã đăng ký thành công!");
				<?php 
				wp_signon( $args ,false );
				$login=triip_login($args); 
				echo "window.location ='".home_url()."';";
				?>
				
			<?php }else{ ?>
				alert("<?php echo $mess ?>");
			<?php } ?>

				
		</script>
		<?php

	}elseif (isset($_POST['submit_logout'])) {
		wp_logout();
		?>
		<script type="text/javascript">
			window.location = "<?php echo home_url() ?>";
		</script>
		<?php
	}elseif(isset($_POST['submit_book'])){
		echo '<pre>'.__FILE__.'::'.__METHOD__.'('.__LINE__.')<br>'; 
			print_r($_POST); 
		echo '</pre>';
		if (is_user_logged_in()) {
			$arr['id_user'] = get_current_user_id();
			$option_key="booked";
			$arr['name'] = $_POST['name'];
			$arr['base'] = $_POST['base-choose'];
			$arr['number_phone'] = $_POST['number'];
			$arr['date_order'] = $_POST['date-order'];
			$arr['time_order'] = $_POST['time-order'];
			$arr['people'] = $_POST['number-human'];
			$arr['message'] = strip_tags($_POST['message_book']);
			//$arr['id_post'] = $_POST['id_post'];
		    $contact =get_option($option_key,false);
		    $arr_mon =array();
		    foreach ($_POST as $key => $value) {
		    	if(is_array($value)==true){
		    		$arr_mon = array_merge($arr_mon,$value);
		    	}
		    }
		    $arr['arr_mon'] = array_unique($arr_mon);

		    if (!$contact) {
		        $contact[time()] = $arr;
		        add_option( 'booked',$contact );
		    }
		    else{
		        $contact[time()] = $arr;
		        update_option( 'booked',$contact);    
		    }
		    global $wp;
		    ?>
		    <script type="text/javascript">
				alert("Bạn đã đặt bàn thành công");
			</script>
		    <?php
			echo ' <script type="text/javascript"> window.location="'.$wp->request.'"; </script> ';
			die();
		}else{
		?>
			<script type="text/javascript">
				alert("Bạn cần đăng nhập để đặt tour!");
			</script>
		<?php
		}
	}
} ?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8"/>
		<meta name="description" content="The description"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, shrink-to-fit=no, user-scalable=no"/>
		<meta name="keywords" content="coding, html, css"/>
		<meta name="author" content="someone"/>
		<!-- Styles-->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"/>
		<link rel="stylesheet" href="<?php echo TFT_URL; ?>/public/libs/bootstrap-3/css/bootstrap-theme.min.css"/>
		<link rel="stylesheet" href="<?php echo TFT_URL; ?>/public/libs/bootstrap-3/css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo TFT_URL; ?>/public/libs/owlcarousel/assets/owl.theme.default.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo TFT_URL; ?>/public/libs/owlcarousel/assets/owl.carousel.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo TFT_URL; ?>/public/libs/owlcarousel/assets/animate.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo TFT_URL; ?>/public/libs/flexslider/flexslider.min.css"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"/>
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"/>
		<link rel="stylesheet" type="text/css" href="<?php echo TFT_URL; ?>/public/style.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<?php wp_head() ?>
	</head>
	<body>
		<div id="wrapper">
			<header id="header">
				<div class="header-top">
					<div class="container-fluid">
						<div class="left float-left">
							<div class="hotline">
								<div class="hotline_img"><img src="<?php echo TFT_URL; ?>/public/images/hotline-icon.png" alt=""/></div>
								<div class="hotline_number">
									<p>18002028</p>
								</div>
							</div>
						</div>
						<div class="right float-right">
							<nav class="menu-top">
								<ul class="list-inline">
									<li class="connect-fb"><a href="#"><img src="<?php echo TFT_URL; ?>/public/images/facebook-icon.png" alt=""/></a></li>
									<li class="book-meal"><a href="#"> 
											<p>Đặt bàn</p></a></li>
									<li class="user">
										<a href="#"> 
											<i class="fas fa-user float-left"></i>
											<p class="float-left">Tài khoản</p>
											<i class="fas fa-angle-down float-left"></i>
											<div class="clear-fix"></div>
										</a>
										<ul class="user-menu2">
											<?php if (!is_user_logged_in()){ ?>
												<li><a href="<?php echo get_page_link( 50 ) ?>">Đăng kí</a></li>
												<li><a href="<?php echo get_page_link( 48 ) ?>">Đăng nhập</a></li>	
											<?php }else{ ?>
												<li><a href="">Xin chào: <?php echo $current_user->user_firstname.' '.$current_user->user_lastname  ?></a></li>
												<li><a href="<?php echo wp_logout_url(home_url()); ?>" id="log_out">Đăng Xuất</a></li>
											<?php } ?>
										</ul>
									</li>
									<li><a href="<?php echo home_url( 'thanh-toan' ) ?>"><i class="fas fa-check float-left"></i>
										<p class="float-left">Thanh toán</p>
										<div class="clear-fix"></div></a></li>
									<li class="cart"><a href="<?php echo home_url( 'gio-hang' ) ?>"><i class="fas fa-shopping-cart float-left"></i>
										<p class="float-left">Giỏ hàng</p>
										<div class="clear-fix"></div></a></li>
								</ul>
							</nav>
						</div>
						<div class="clear-fix"></div>
					</div>
				</div>
				<div class="header-bottom">
					<div class="container-fluid">
						<div class="logo float-left"> 
							<div class="logo-img"><a href="<?php echo home_url() ?>"><img src="<?php echo TFT_URL; ?>/public/images/logo.png" alt=""/></a></div>
						</div>
						<nav class="menu-bottom float-right">
							<ul class="list-inline">
								<li><a href="#">Trang chủ</a></li>
								<li><a href="#">Ưu đãi </a></li>
								<li><a href="#">Giới thiệu</a></li>
								<li><a href="#">
										 Thực dơn chọn món<i class="fas fa-sort-down"></i></a>
									<ul class="dropdown-menu2">
										<li><a href="#">Thức Ăn</a></li>
										<li><a href="#">Thức Ăn</a></li>
										<li><a href="#">Thức Ăn</a></li>
										<li><a href="#">Thức Ăn</a></li>
										<li><a href="#">Bánh tráng Trảng Bàng</a></li>
									</ul>
								</li>
								<li><a href="#">Sét Menu<i class="fas fa-sort-down"></i></a>
									<ul class="dropdown-menu2">
										<li><a href="#">Thức Ăn</a></li>
										<li><a href="#">Thức Ăn</a></li>
										<li><a href="#">Thức Ăn</a></li>
										<li><a href="#">Thức Ăn</a></li>
										<li><a href="#">Bánh tráng Trảng Bàng</a></li>
									</ul>
								</li>
								<li><a href="#">Ẩm thực Phương nam</a></li>
								<li class="search-form">
									<form id="search" action="">
										<input type="text" placeholder="Tìm kiếm"/>
										<button type="submit"></button>
									</form>
								</li>
								<li class="cart"><a href="#" style="background-image: url(<?php echo TFT_URL ?>/public/images/basket-icon.png);"></a>
									<div class="cart-hover cart-hover-pc">
										<div class="content">
											<div class="text-center">
												<p>Giỏ hàng của bạn đang trống</p><a href="#">Tiếp tục mua hàng</a>
											</div>
										</div>
										<div class="content active">
											<?php 
											global $woocommerce;
											$items = $woocommerce->cart->get_cart();
											$tong = 0;
											foreach($items as $item => $values) { 
												$_product =  wc_get_product( $values['data']->get_id()); 
												$price = get_post_meta($values['product_id'] , '_price', true);
												$tong += ($price*$values['quantity']);
												$img_avt = get_the_post_thumbnail_url( $values['product_id'] );
												$img_avt = !$img_avt?(TFT_URL.'/no_photo.jpg'):$img_avt;
												?>
												<div class="cart-item active">
													<div class="cart-item_img float-left"><a href="<?php echo $_product->get_ID(); ?>"><img src="<?php echo $img_avt ?>" alt=""/></a></div>
													<div class="cart-item_text float-left"><a class="name" href="#"><?php echo $_product->get_title() ?></a>
														<p class="price"><span id="quantity"><?php echo $values['quantity'] ?> </span>x <span id="price"><?php echo wc_price($price) ?></span></p>
													</div>
													<div class="cart-remove">
														<div id="cart-remove_item" the-id="<?php echo $values['key'] ?>" class="remove-cart">
															<i class="fas fa-times"></i>
														</div>
													</div>
													<div class="clear-fix"></div>
												</div>
												<?php
											}
											?>
											
											<div class="cart-price">
												<p> <span>Tổng tiền: </span><?php echo  wc_price($tong); ?> </p>
											</div>
											<div class="cart-act"><a class="button cart-bag" href="<?php echo home_url( 'gio-hang' ) ?>">Giỏ hàng</a><a class="button cart-bag" href="<?php echo home_url( 'thanh-toan' ) ?>">Thanh Toán</a></div>
											<script type="text/javascript">
												$('.remove-cart').click(function(event) {
													var product_id =$(this).attr("the-id");
													$.ajax({
														type: 'POST',
														data: {
														    action: 'remove-cart-item',
														    cartid : product_id,
														    nonce: WPAjax.nonce,
														},
														url: WPAjax.url,
														beforeSend: function() {

														},
														success: function(data) {
														    alert("Xóa khỏi giỏ hàng thành công");
														    // load_cart_again = true;
														    load_cart();
														    // window.location = window.location;
														}
													});
												});
											</script>
										</div>
									</div>
								</li>
							</ul>
						</nav>
						<div class="clear-fix"></div>
					</div>
				</div>
				<div class="header-mobie">
					<div class="container">
						<div class="row">
							<div class="logo">
								<div class="logo_img text-center"><a href="<?php echo home_url(); ?>"><img src="<?php echo TFT_URL; ?>/public/images/ta_history-logo.png" alt=""/></a></div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="menu-mobie fleft"><a href="#"><i class="fas fa-bars"></i></a></div>
								<div class="logo-mobie text-center"><img src="<?php echo TFT_URL; ?>/public/images/ta_history-logo.png" alt=""/></div>
								<div class="right-mobie fright">
									<ul class="list-inline">
										<li class="user"><a href="#"><i class="fas fa-user"></i></a>
											<ul class="user-drop">
												<li><a href="#">Đăng kí</a></li>
												<li><a href="#">Đăng nhập</a></li>
												<li><a href="#">Thanh toán</a></li>
												<li><a href="#">Giỏ hàng</a></li>
											</ul>
										</li>
										<li class="search"><a href="#"><i class="fas fa-search"></i></a>
											<div class="search-drop">
												<form id="search-mobie" action="">
													<input type="text" placeholder="Tìm kiếm"/>
													<button type="submit"></button>
												</form>
											</div>
										</li>
										<li class="cart"><a href="#"><i class="fas fa-shopping-cart"></i></a>
											<div class="cart-hover">
												<div class="content">
													<div class="text-center">
														<p>Giỏ hàng của bạn đang trống</p><a href="#">Tiếp tục mua hàng</a>
													</div>
												</div>
												<div class="content active">
													<div class="cart-item active">
														<div class="cart-item_img float-left"><a href="#"><img src="<?php echo TFT_URL; ?>/public/images/ta_cart_img_item.jpg" alt=""/></a></div>
														<div class="cart-item_text float-left"><a class="name" href="#">Bánh xèo tôm thịt</a>
															<p class="price"><span id="quantity">1 </span>x <span id="price">75,000 </span><span>₫</span></p>
														</div>
														<div class="cart-remove">
															<div id="cart-remove_item"><i class="fas fa-times"></i></div>
														</div>
														<div class="clear-fix"></div>
													</div>
													<div class="cart-price">
														<p> <span>Tổng tiền: </span><span>75,000<sup>₫</sup></span></p>
													</div>
													<div class="cart-act"><a class="button cart-bag" href="#">Giỏ hàng</a><a class="button cart-bag" href="#">Thanh Toán</a></div>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div class="clear-fix"></div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div class="header-fixed text-center">
				<div class="header-fixed__logo"><a href="<?php echo home_url() ?>"><img src="<?php echo TFT_URL; ?>/public/images/ta_history-logo.png" alt=""/></a></div>
				<div class="bar-menu"></div>
				<div class="header-fixed__nav">
					<nav class="menu-left">
						<ul class="menu-left__1">
							<li><a href="#">Trang chủ </a></li>
							<li class="list-drop"><a href="#">Thực đơn món ăn</a><span class="drop-button"></span>
								<ul class="menu-left__2">
									<li><a href="#">Set menu</a></li>
									<li><a href="#">Cuốn</a></li>
									<li><a href="#">Lẩu </a></li>
									<li><a href="#">Nướng </a></li>
									<li><a href="#">Lạ miệng</a></li>
								</ul>
							</li>
							<li><a href="#">Ưu đãi </a></li>
							<li><a href="#">Giới thiệu </a></li>
							<li><a href="#">Liên hệ</a></li>
						</ul>
					</nav>
				</div>
			</div>