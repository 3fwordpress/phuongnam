<?php 
/**
 * Archive file, the file show all post in a category
 * @author 3F Wordpress Team
 * @link http://3fgroup.vn
 */
?>

<?php get_header(); ?>
      

<section class="ATPN" id="ATPN">
	<div class="topbar topbar-wrap">
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<ul class="list-inline">
							<li> <a href="#">Trang chủ</a></li>
							<li class="li-display"><a href="#">Danh mục</a></li>
							<li><a href="#">Blog - Ẩm thực Phương Nam</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--.topbar-wrap-->
	<div class="goodsale-content">
		<div class="container">
			<div class="row">
				<div class="main-content col-sm-12">
					<div class="row">
						<div class="col-lg-3 col-sm-12 col-3">
							<aside class="sidebar listmenu1">
								<div class="listmenu2 listmenu2-wrap">
									<div class="listmenu2__title">
										<h4>Phương Nam</h4>
									</div>
									<div class="listmenu2__content">
										<ul class="list-menu">
											<li><a href="#">Trang chủ</a></li>
											<li><a href="#">Ưu đãi</a></li>
											<li><a href="#">Giới thiệu</a></li>
											<li>
												<a href="#">Thực đơn chọn món<i class="arrow-down"></i></a>
												<ul class="list-menu__2">
													<li><a href="#">Đồ nướng</a></li>
													<li><a href="#">Đồ uống</a></li>
													<li><a href="#">Món nóng</a></li>
													<li><a href="#">Các món lẩu</a></li>
													<li><a href="#">Món ăn nhẹ</a></li>
													<li><a href="#">Món ngon nổi bật</a></li>
													<li><a href="#">Cuốn phương Nam</a></li>
													<li><a href="#">Bánh Tráng Trảng Bàng</a></li>
												</ul>
											</li>
											<li>
												<a href="#">Set menu<i class="arrow-down"></i></a>
												<ul class="list-menu__2">
													<li><a href="#">Hẹn hò</a></li>
													<li><a href="#">Sum vầy</a></li>
													<li><a href="#">Họp mặt</a></li>
												</ul>
											</li>
											<li><a href="#">Ẩm thực Phương Nam</a></li>
										</ul>
									</div>
								</div>
								<!--.listmenu2-wrap-->
								<div class="lst-item lst-item-wrap">
									<div class="listmenu__title">
										<h4>Bài viết mới nhất</h4>
									</div>
									<div class="lst-item__content">
                                        <?php $new_post = new WP_Query(array( 'post_type' => 'post')) ?>
                                        <?php while (have_posts()) : the_post();?> 
										<div class="item-sidebar item-sidebar-wrap">
											<div class="seller-item">
												<div class="seller-img"><a href="<?php the_permalink() ?>"><img src="<?php the_post_thumbnail_url( )?>" alt=""/></a></div>
												<div class="product-content">
													<h5><a href="<a href="<?php the_permalink() ?>"><?php the_title() ?></a></h5>
													<span><?php the_time('d/m/y') ?></span>
												</div>
												<div class="clear-fix"></div>
											</div>
                                        </div>
                                        <?php endwhile;?> 
									</div>
								</div>
							</aside>
						</div>
						<div class="col-lg-9 col-sm-12 col-9">
							<div class="col9-content">
								<div class="title">
									<h4>Ẩm thực Phương Nam</h4>
								</div>
								<div class="list-news">
                                    <?php if (have_posts()):

                                    while (have_posts()) : the_post();?> 
                                       <div class="list-new-item list-new-item-wrap">
                                            <div class="row">
                                                <div class="col-sm-5 col-xs-12">
                                                    <div class="col-box"><img src="<?php the_post_thumbnail_url() ?>" alt=""/></div>
                                                </div>
                                                <div class="col-sm-7 col-xs-12">
                                                    <div class="col-box">
                                                        <a href="<?php the_permalink() ?>">
                                                            <h2><?php the_title() ?></h2>
                                                        </a>
                                                        <div class="body-listnews">
                                                            <small><?php the_time( 'd-m/y' ) ?></small>
                                                            <p><?php the_excerpt() ?></p>
                                                            <a href="<?php the_permalink() ?>"><i>Xem tiếp&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></i></a>
                                                            <div class="postDetail">
                                                                <ul class="list-inline">
                                                                    <li><i class="fa fa-file-text-o" aria-hidden="true"></i></li>
                                                                    <?php $cats = get_the_category();
                                                                    foreach ($cats as $item) {
                                                                    ?>
                                                                    <li><a href="<?php get_category_link( $item->term_id ); ?> "><span><?php echo $item->name ?></span></a></li>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile;?> 

                                    <?php else : ?>
                                    <h3><?php _e('Không có bài viết nào được tìm thấy.'); ?></h3>
                                    <?php endif; ?>
									
								</div>
								<div class="Pagination">
									<div class="col-lg-2 col-md-2 col-sm-3"><a class="prev" href="#"><span><i class="fa fa-angle-left"></i>&nbsp;Trang trước</span></a></div>
									<div class="col-lg-8 col-md-8 col-sm-6">
										<ul class="list-inline">
											<li><a class="a-active" href="#">1</a></li>
											<li><a href="#">2</a></li>
											<li><a href="#">3</a></li>
											<li><a href="#">4</a></li>
											<li class="dots"><a href="#">...</a></li>
											<li><a href="#">10</a></li>
										</ul>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-3 text-right"><a class="next" href="#"><span>Trang sau&nbsp;<i class="fa fa-angle-right"></i></span></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="up up-active"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
</section>



<?php get_footer(); ?>