<?php 
add_filter( 'woocommerce_currencies', 'add_my_currency' );
function add_my_currency( $currencies ) {
	$currencies['vnd'] = __( 'Việt Nam Đồng', 'woocommerce' );
	return $currencies;
}
add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);
function add_my_currency_symbol( $currency_symbol, $currency ) {
	switch( $currency ) {
	case 'vnd': $currency_symbol = 'VNĐ'; break;
	}
	return $currency_symbol;
}

function triip_login($args){

	if (!is_user_logged_in()) {
		$user = wp_signon( $args, false );
		return $user;
	}
	return false;
}


add_action('wp_footer', 'get_custom_coupon_code_to_session');
function get_custom_coupon_code_to_session(){
    if( isset($_GET['coupon_code']) ){
        $coupon_code = WC()->session->get('coupon_code');
        if(empty($coupon_code)){
            $coupon_code = esc_attr( $_GET['coupon_code'] );
            WC()->session->set( 'coupon_code', $coupon_code ); // Set the coupon code in session
        }
    }
}

add_action( 'woocommerce_before_checkout_form', 'add_discout_to_checkout', 10, 0 );
function add_discout_to_checkout( ) {

    $coupon_code = WC()->session->get('coupon_code');
    if ( ! empty( $coupon_code ) && ! WC()->cart->has_discount( $coupon_code ) ){
        WC()->cart->add_discount( $coupon_code ); // apply the coupon discount
        WC()->session->__unset('coupon_code'); // remove coupon code from session
    }
}

function register_user($args,$name){

	$new_user = wp_create_user( $args['email'], $args['password'], $args['email']);
	if ( is_wp_error($new_user) ){
		return $new_user->get_error_message();
	}else{
		wp_update_user( array(
			'ID' => $new_user,
			'user_nicename' => $args[ 'name' ],
			'role' => 'author',
			'first_name' => $name['first_name'],
			'last_name' => $name['last_name']
		));
		return true;
	}
}

function get_family_terms($taxonomy_name){// lấy mảng các term có quan hệ cha con với nhau
    $terms = get_terms( $taxonomy_name, array(
            'hide_empty' => false,
    ) );
    $parent=array();
    foreach ($terms as $value) {
        if($value->parent==0){
            $parent[$value->term_id]['name']=$value->name;
            $parent[$value->term_id]['slug']=$value->slug;
            $parent[$value->term_id]['id']=$value->term_id;
        }else{
            $parent[$value->parent]['children'][$value->term_id]=$value;
        }
    }
    foreach ($parent as $key => $value) {
        if(isset($value['children'])){
            foreach ($value['children'] as $key_2 => $value_2) {
                if(array_key_exists($key_2,$parent)==1){
                    $value_2->children = $parent[$value_2->term_id]['children'];
                    unset($parent[$value_2->term_id]);
                }
            }
        }
    }
    return $parent;
}
function path_from_province_sigle()
{
	// $_id_post= get_the_ID();
	$_term = get_the_terms(get_the_ID(),'product_cat');
	$_post = get_post( get_the_ID() );	
	if (isset($_term[0]->parent)) {
		$_p_term = get_term($_term[0]->parent,'product_cat');
		?>
		<a href="<?php echo get_term_link( $_p_term ) ?>"><?php echo $_p_term->name ?></a> |
		<?php
	}
	?>
		<a href="<?php echo get_term_link( $_term[0] ) ?>"><?php echo $_term[0]->name ?></a> | <span><?php echo $_post->post_title ?></span>
	<?php
}

function path_from_province_archive()
{
	// $_id_post= get_the_ID();
	$_term = get_queried_object();
	if (($_term->parent)!=0) {
		$_p_term = get_term($_term->parent,'product_cat');
		echo '<pre>'.__FILE__ .'::'.__METHOD__ .'('.__LINE__ .')<br>';
			print_r($_p_term);
		echo '</pre>';
		?>
		<a href="<?php echo get_term_link( $_p_term ) ?>"><?php echo $_p_term->name ?></a> |
		<?php
	}
	?>
		<span><?php echo get_queried_object()->name ?></span>
	<?php

}

function set_count_view($post_id){
	$value =get_post_meta( $post_id, 'count_view',true);
	if (empty($value)) {
		add_post_meta( $post_id, 'count_view', 1);
	}else{
		$value++;
		update_post_meta( $post_id, 'count_view', $value );
	}
	return get_post_meta( $post_id, 'count_view',true);
}


function count_view($post_id,&$time_watched){
	//$_SESSION['time_visited'];
	$min_minutes = time()-$time_watched;
	if ($min_minutes>300) {
		set_count_view($post_id);
		$time_watched = time();
	}else{
		return ;
	}
}

function mt_destination_navigate($permalink_structure = true, $query = null, $link = null) {
	global $wp_query, $wp;
	if($query == null) {
		$query = $wp_query;
	}
	$post_of_page = $query->query_vars['posts_per_page'];
	$total = $query->max_num_pages;
	$current_page = $query->query_vars['paged'];
	if($link==null) {
		$format = ($permalink_structure)?'/page/':'?paged=';
		$link = get_term_link(get_queried_object(), 'destination').$format;
		$current_page = $query->query_vars['paged'];
	}
	if ( $total > 1 )  { ?>
	<ul class="list-inline">
		<?php
		if($current_page >1 )
			echo '<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a class="prev" href="'.$link.($current_page-1).'"><span><i class="fa fa-angle-left"></i>&nbsp;Trang trước</span></a></div>';
			// echo '<li><a href="'.$link.($current_page-1).'"><i class="fas fa-chevron-left"></i></a></li>';
		else
			echo '';
		?>
		<?php for($i=1; $i<=$total; $i++) {
			if($i==$current_page)
				echo '<li><a class="a-active" href="#">'.$i.'</a></li>';
			else
				echo '<li><a href="'.$link.$i.'">'.$i.'</a></li>';
		}
		if($current_page < $total)
			echo '<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 text-right"><a class="next" href="'.$link.($current_page+1).'"><span>Trang sau&nbsp;<i class="fa fa-angle-right"></i></span></a></div>';
		else
			echo '';
		?>
	</ul>
	<?php }
}
?>