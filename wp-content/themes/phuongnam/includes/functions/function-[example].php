<?php 
 //function create breakcrumb

 if(!function_exists('tw_the_breadcrumb')) {
  function tw_the_breadcrumb() {
      echo '<ul id="crumbs">';
      if (!is_home()) {
          echo '<li><a href="';
          echo get_option('home');
          echo '">';
          echo '<i class="fa fa-home" aria-hidden="true"></i> ' . __('Trang chủ','twtheme');
          echo "</a></li>";
          if (is_category() || is_single()) {
                  echo '<li><a href="'.home_url('news').'">'.__('Tin tức', 'twtheme').'</a></li><li>';
                  the_category(' </li><li> ');
                  if (is_single()) {
                          echo "</li><li>";
                          the_title();
                          echo '</li>';
                  }
          } elseif (is_page()) {
                  echo '<li>';
                  echo the_title();
                  echo '</li>';
          }
      }
      elseif (is_tag()) {single_tag_title();}
      elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
      elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
      elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
      elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
      elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
      elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
      echo '</ul>';
  }
}


/**
 * ================Example Functions======================
 * Get ACF map: acf_get_google_map(<location from get_field>)
 * 
 */