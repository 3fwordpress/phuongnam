<?php 

add_action( 'wp_ajax_ajax_delete_contact','ajax_delete_contact' );
add_action( 'wp_ajax_nopriv_ajax_delete_contact','ajax_delete_contact' );
add_action( 'wp_ajax_ajax_delete_booked','ajax_delete_booked' );
add_action( 'wp_ajax_nopriv_ajax_delete_booked','ajax_delete_booked' );
add_action( 'wp_ajax_send_mail_user','send_mail_userx' );
add_action( 'wp_ajax_nopriv_send_mail_user','send_mail_userx' );


function ajax_delete_contact(){
    $option_key="data_contact";
    $contact =get_option($option_key,false);
    $arr_check=( $_POST['key'] );
    foreach ($arr_check as  $value) {
        unset($contact[$value]);
    }
    if (empty($contact)) {
            delete_option('data_contact');
        }
    else{
        update_option( 'data_contact',$contact);
    }
    echo json_encode($contact);
    die();
}


function ajax_delete_booked(){
    $option_key="booked";
    $booked =get_option($option_key,false);
    $arr_check=( $_POST['key'] );
    foreach ($arr_check as  $value) {
        unset($booked[$value]);
    }
    if (empty($booked)) {
            delete_option('booked');
        }
    else{
        update_option( 'booked',$booked);
    }
    echo json_encode($booked);
    die();
}

function send_mail_userx(){
    $users= get_users();
    foreach ($users as $user) {
        $data =$user->data;
        send_a_mail_to_user($data->user_email,$data->display_name);        
    }
    echo "ahihi";
    die();
}

function send_a_mail_to_user($to_mail,$name)
{
    $subject =get_field('travic_subject','option');
    $message=get_field('notification_mail','option');
    $message =str_replace( 
        array('{{ name }}','{{name}}','{{name }}','{{ name}}'),
        array($name, $name, $name, $name), $message );

    wp_mail( $to_mail, $subject, $message );
}



?>