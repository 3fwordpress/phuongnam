<?php
function tao_taxonomy() {
	/* Biến $label chứa các tham số thiết lập tên hiển thị của Taxonomy
	*/
	$labels = array(
		'name' => 'Thực đơn',
		'singular' => 'Thực đơn',
		'menu_name' => 'Quản lý thực đơn'
	);

	/* Biến $args khai báo các tham số trong custom taxonomy cần tạo
	*/
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,//true giống chuyên mục, false giống thẻ ( tag )
		'public'                     => true,//
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	/* Hàm register_taxonomy để khởi tạo taxonomy */
	register_taxonomy('thuc-don', 'product', $args);

}

// Hook into the 'init' action
add_action( 'init', 'tao_taxonomy', 0 );