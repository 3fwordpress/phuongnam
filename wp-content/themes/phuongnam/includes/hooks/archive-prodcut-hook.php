<?php 

add_action( 'archive_product_left_hook', 'archive_product_left_cat',10 );
add_action( 'archive_product_left_hook', 'archive_product_left_new',15 );



function archive_product_left_cat()
{
?>
<div class="listmenu listmenu-wrap">
	<?php 
	$arrr = get_family_terms('product_cat');
	?>
	<div class="listmenu__title">
		<h4>Thực đơn chọn món</h4>
	</div>
	<ul class="list-menu">
		<?php foreach ($arrr as $key => $value): ?>
			<li><a href="<?php echo get_term_link( $value['id'] ) ?>"><?php echo $value['name'] ?></a></li>	
		<?php endforeach ?>
	</ul>
</div>
<?php
}
function archive_product_left_new()
{
?>
<div class="lst-item lst-item-wrap">
	<div class="listmenu__title">
		<h4>Món ngon nổi bật</h4>
	</div>
	<div class="owl-carousel">
		<div class="lst-item__content">
			<div class="item-sidebar2 item-sidebar2-wrap">
				<div class="seller-item">
					<div class="seller-img"><a href="#"><img src="images/anh1.jpg" alt=""/></a></div>
					<div class="product-content">
						<h5><a href="#">Bánh Tráng Trảng Bàng Cuốn Chân Giò Luộc</a></h5>
						<h3 class="price"><span>155,000</span><span><sup>
									<u>đ</u></sup></span></h3>
					</div>
					<div class="clear-fix"></div>
				</div>
			</div>
			<!--.item-sidebar2-wrap-->
			<div class="item-sidebar2 item-sidebar2-wrap">
				<div class="seller-item">
					<div class="seller-img"><a href="#"><img src="images/anh1.jpg" alt=""/></a></div>
					<div class="product-content">
						<h5><a href="#">Bánh Tráng Trảng Bàng Cuốn Chân Giò Luộc</a></h5>
						<h3 class="price"><span>155,000</span><span><sup>
									<u>đ</u></sup></span></h3>
					</div>
					<div class="clear-fix"></div>
				</div>
			</div>
			<!--.item-sidebar2-wrap-->
		</div>
		<div class="lst-item__content">
			<div class="item-sidebar2 item-sidebar2-wrap">
				<div class="seller-item">
					<div class="seller-img"><a href="#"><img src="images/anh1.jpg" alt=""/></a></div>
					<div class="product-content">
						<h5><a href="#">Bánh Tráng Trảng Bàng Cuốn Chân Giò Luộc</a></h5>
						<h3 class="price"><span>155,000</span><span><sup>
									<u>đ</u></sup></span></h3>
					</div>
					<div class="clear-fix"></div>
				</div>
			</div>
			<!--.item-sidebar2-wrap-->
			<div class="item-sidebar2 item-sidebar2-wrap">
				<div class="seller-item">
					<div class="seller-img"><a href="#"><img src="images/anh1.jpg" alt=""/></a></div>
					<div class="product-content">
						<h5><a href="#">Bánh Tráng Trảng Bàng Cuốn Chân Giò Luộc</a></h5>
						<h3 class="price"><span>155,000</span><span><sup>
									<u>đ</u></sup></span></h3>
					</div>
					<div class="clear-fix"></div>
				</div>
			</div>
			<!--.item-sidebar2-wrap-->
		</div>
		<div class="lst-item__content">
			<div class="item-sidebar2 item-sidebar2-wrap">
				<div class="seller-item">
					<div class="seller-img"><a href="#"><img src="images/anh1.jpg" alt=""/></a></div>
					<div class="product-content">
						<h5><a href="#">Bánh Tráng Trảng Bàng Cuốn Chân Giò Luộc</a></h5>
						<h3 class="price"><span>155,000</span><span><sup>
									<u>đ</u></sup></span></h3>
					</div>
					<div class="clear-fix"></div>
				</div>
			</div>
			<!--.item-sidebar2-wrap-->
			<div class="item-sidebar2 item-sidebar2-wrap">
				<div class="seller-item">
					<div class="seller-img"><a href="#"><img src="images/anh1.jpg" alt=""/></a></div>
					<div class="product-content">
						<h5><a href="#">Bánh Tráng Trảng Bàng Cuốn Chân Giò Luộc</a></h5>
						<h3 class="price"><span>155,000</span><span><sup>
									<u>đ</u></sup></span></h3>
					</div>
					<div class="clear-fix"></div>
				</div>
			</div>
			<!--.item-sidebar2-wrap-->
		</div>
	</div>
</div>
<?php
}