<?php 
add_action( 'phuong_nam_home_hook', 'home_hook_banner', 10 );
add_action( 'phuong_nam_home_hook', 'home_hook_product_by_cat', 15 );
add_action( 'phuong_nam_home_hook', 'home_hook_product_banner_page', 20 );
add_action( 'phuong_nam_home_hook', 'home_hook_discount_product', 25 );
add_action( 'phuong_nam_home_hook', 'home_hook_custommer_comment', 30 );
add_action( 'phuong_nam_home_hook', 'home_hook_partner', 35 );
// add_action( 'phuong_nam_home_hook', 'home_hook_', 40 );
// add_action( 'phuong_nam_home_hook', 'home_hook_', 45 );
// add the action 
add_action( 'woocommerce_after_checkout_form', 'action_woocommerce_after_checkout_form', 10, 1 ); 

// define the woocommerce_after_checkout_form callback 
function action_woocommerce_after_checkout_form( $checkout ) { 
    ?>
    <h2 style="text-align: center;">bạn đã đặt mua thành công. qua lại <a href="<?php echo home_url() ?>">Trang chủ</a> để mua hàng</h2>
    <?php
}; 
         


function home_hook_banner()
{
	$list_imgs_id = get_field('pn_home_banner',get_the_ID(),false);
?>
	<div class="banner-slide">
		<div class="container-fluid">
			<div class="row">
				<div class="owl-carousel owl-theme">
				<?php foreach ($list_imgs_id as $value): ?>
					<?php $url_img = wp_get_attachment_image_url( $value ); ?>
					<div class="item">
						<div class="banner-slider__img"><img src="<?php echo $url_img ?>" alt=""/></div>
					</div>
				<?php endforeach ?>
				</div>
			</div>
		</div>
	</div>
<?php
}

function home_hook_product_by_cat()
{
	$product_terms = get_terms( 'product_cat',array(
        'hide_empty' => false,
    ));
?>
	<section class="menu" id="menu">
		<div class="container">
			<div class="row">
				<div class="menu__img text-center"><img src="<?php echo TFT_URL; ?>/public/images/our-menu.png" alt=""/></div>
				<div class="menu__title text-center">
					<h1>Thực đơn của nhà hàng</h1>
				</div>
				<div class="menu__info text-center">
					<p> <i><?php echo get_field( 'pn_menu_home_content',false); ?></i></p>
				</div>
				<div class="menu__tab-product">
					<ul class="nav nav-pills text-center list-inline">
						<?php $count =1; ?>
						<?php foreach ($product_terms as $key => $value): ?>
							<li <?php echo $count==1?'class="active"':''; ?>><a href="#<?php echo $value->slug ?>" data-toggle="tab"><?php echo $value->name ?></a></li>
							<?php $count++; ?>	
						<?php endforeach ?>
					</ul>
					<div style="ad"> </div>
					<script type="text/javascript">
					</script>
					<div class="tab-content clearfix">
						<?php $count =1; ?>
						<?php 
						foreach ($product_terms as $key => $value){ ?>

						<div class="tab-pane <?php echo $count==1?'active':''; ?>" id="<?php echo $value->slug ?>">
								<?php 
								$args = array(
									'post_type' => 'product',
									'tax_query' => array(
										array(
											'taxonomy' => $value->taxonomy,
											'field' => 'term_id',
											'terms' => $value->term_id
										)
									)
								); 
								$_posts = get_posts( $args );
								if (!empty($_posts)){ ?>
								<div class="owl-carousel owl-theme">
									<?php
									foreach ($_posts as $key_2 => $value_2){ ?>
										<?php $product = wc_get_product( $value_2->ID ); ?>
										<div class="item">
											<div class="product product-wrap component">
												<div class="product-info"><a href="<?php echo get_permalink( $value_2->ID ) ?>">
														<div class="product-info__img"><img src="<?php echo get_the_post_thumbnail_url( $value_2->ID ) ?>" alt=""/></div>
														<div class="product-info__act">
															<ul class="list-inline">
																<li class="add-cart">
																	<a class="button add_to_cart" data-toggle="modal" the-id="<?php echo $value_2->ID ?>" data-target="#add-cart-popup"><span class="">Thêm vào giỏ</span></a>
																</li>
																<li class="info">
																	<a class="img" href="<?php echo get_permalink( $value_2->ID  ) ?>"></a>
																</li>
																<li class="view">
																	<span class="img view-popupx" data-toggle="modal" data-target="#view-product-popup" the-id="<?php echo $value_2->ID ?>"></span>
																</li>
															</ul>
														</div></a>
													</div>
												<div class="product-box">
													<div class="product-box__name fleft"><a href="#"><?php echo $product->get_title() ?></a></div>
													<div class="product-box__price fright">
														<h4><?php echo wc_price($product->get_price()); ?></h4>
													</div>
													<div class="clear-fix"></div>
												</div>
											</div>
										</div>
										<?php //echo wc_price($product->get_price());
									}
									?>
								</div> <?php 
								}
								else{ ?>
									<h2 style="text-transform: uppercase;font-weight: bold;width: 100%;text-align: center;">Không có sản phẩm nào trong danh mục này!</h2>
								<?php }
								$count++; ?>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
}

function home_hook_product_banner_page()
{
?>
<section class="banner-home" id="banner-home">
	<div class="container">
		<div class="row">
			<ul class="list-item item-left col-xs-3">
				<li><a href="#"><img src="<?php echo TFT_URL; ?>/public/images/banner-3-copy.jpg" alt=""/></a></li>
				<li><a href="#"><img src="<?php echo TFT_URL; ?>/public/images/banner-4-copy.jpg" alt=""/></a></li>
			</ul>
			<div class="item-center col-xs-6"><img src="<?php echo TFT_URL; ?>/public/images/banner-1-copy.jpg" alt=""/></div>
			<ul class="list-item item-right col-xs-3">
				<li><a href="#"><img src="<?php echo TFT_URL; ?>/public/images/banner-3.jpg" alt=""/></a></li>
				<li><a href="#"><img src="<?php echo TFT_URL; ?>/public/images/banner-4.jpg" alt=""/></a></li>
			</ul>
		</div>
	</div>
</section>
<?php
}

function home_hook_discount_product()
{
?>
<section class="product-sale" id="product-sale">
	<div class="container">
		<div class="row">
			<div class="product-sale__title text-center">
				<h1>Sản Phẩm Khuyễn Mãi</h1>
			</div>
			<div class="product-sale__content text-center">
				<p> <i><?php echo get_field( 'pn_home_promotional_products',false); ?></i></p>
			</div>
			<div class="product-sale__slide">
				<div class="owl-carousel owl-theme">
					<?php 
					$args = array(
						'posts_per_page'   => 8,
						'offset'           => 0,
						'orderby'          => 'date',
						'order'            => 'DESC',
						'post_type'        => 'product',
					);
					$posts_array = get_posts( $args );
					// echo '<pre>'.__FILE__.'::'.__METHOD__.'('.__LINE__.')<br>'; 
					// 	print_r($posts_array);
					// echo '</pre>';
					?>
					<?php
					foreach ($posts_array as $key => $value): ?>
					<?php 
					$product = wc_get_product( $value->ID );
					// echo '<pre>'.__FILE__.'::'.__METHOD__.'('.__LINE__.')<br>'; 
					// 	print_r($product); 
					// echo '</pre>'; 
					?>
					<div class="item">
						<div class="product product-wrap component">
							<div class="product-info"><a href="#">
									<div class="product-info__img"><img src="<?php echo get_the_post_thumbnail_url( $value->ID ) ?>" alt=""/></div>
									<div class="product-info__act">
										<ul class="list-inline">
											<li class="add-cart">
												<a class="button add_to_cart" data-toggle="modal" the-id="<?php echo $value->ID ?>" data-target="#add-cart-popup"><span class="">Thêm vào giỏ</span></a>
											</li>
											<li class="info"><a class="img" href="<?php echo get_permalink( $value_2->ID  ) ?>"></a></li>
											<li class="view">
												<span class="img view-popupx" data-toggle="modal" data-target="#view-product-popup" the-id="<?php echo $value->ID ?>"></span>
											</li>
										</ul>
									</div></a></div>
							<div class="product-box">
								<div class="product-box__name fleft"><a href="#"><?php echo $product->get_title() ?></a></div>
								<div class="product-box__price fright">
									<h4><?php echo wc_price($product->get_price()); ?></h4>
								</div>
								<div class="clear-fix"></div>
							</div>
						</div>
					</div>
					<?php endforeach ?>
				</div>
					
			</div>
		</div>
	</div>
</section>
<?php
}
//pn_home_partners

function home_hook_custommer_comment()
{
$_list = get_field( 'pn_home_customer_comments',false);
?>
<section class="customer-cmt" id="customer-cmt" style="background: url(<?php echo TFT_URL; ?>/public/images/ta_testimonial-bg.jpg) repeat fixed center center;">
	<div class="container">
		<div class="row">
			<div class="customer-cmt__slide">
				<div class="customer-cmt__icon text-center"><img src="<?php echo TFT_URL; ?>/public/images/ta_customer-icon.png" alt=""/></div>
				<div class="customer-cmt__title text-center">
					<h3>Nhận xet khách hàng</h3>
				</div>
				<div class="owl-carousel owl-theme text-center">
					<?php foreach ($_list as $key => $value): ?>
					<div class="item">
						<div class="customer-cmt__avatar"><img src="<?php echo $value['image']['url'] ?>" alt=""/></div>
						<div class="customer-cmt__text">
							<p><?php echo $value['name'] ?></p>
						</div>
					</div>
					<?php endforeach ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
}

function home_hook_partner()
{
	$_list = get_field( 'pn_home_partners',false);
?>
<section class="partner" id="partner">
	<div class="container">
		<div class="row text-center">
			<div class="partner__icon"><img src="<?php echo TFT_URL; ?>/public/images/our-blog.png" alt=""/></div>
			<div class="partner__title">
				<h3>Đối tác</h3>
			</div>
			<div class="partner__list">
				<div class="owl-carousel owl-theme text-center">
					<?php foreach ($_list as $key => $value): ?>
						<div class="item"><img src="<?php echo $value['img']['url'] ?>" alt="<?php echo $value['name'] ?>"/></div>
					<?php endforeach ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
}