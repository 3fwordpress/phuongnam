<?php 
global $woocommerce;
$items = $woocommerce->cart->get_cart();
$tong = 0;
foreach($items as $item => $values) { 
	$_product =  wc_get_product( $values['data']->get_id()); 
	$price = get_post_meta($values['product_id'] , '_price', true);
	$tong += ($price*$values['quantity']);
	$img_avt = get_the_post_thumbnail_url( $values['product_id'] );
	$img_avt = !$img_avt?(TFT_URL.'/no_photo.jpg'):$img_avt;
	?>
	<div class="cart-item active">
		<div class="cart-item_img float-left"><a href="<?php echo $_product->get_ID(); ?>"><img src="<?php echo $img_avt ?>" alt=""/></a></div>
		<div class="cart-item_text float-left"><a class="name" href="#"><?php echo $_product->get_title() ?></a>
			<p class="price"><span id="quantity"><?php echo $values['quantity'] ?> </span>x <span id="price"><?php echo wc_price($price) ?></span></p>
		</div>
		<div class="cart-remove">
			<div id="cart-remove_item" the-id="<?php echo $values['key'] ?>" class="remove-cart">
				<i class="fas fa-times"></i>
			</div>
		</div>
		<div class="clear-fix"></div>
	</div>
	<?php
}
?>
	
	<div class="cart-price">
		<p> <span>Tổng tiền: </span><?php echo  wc_price($tong); ?> </p>
	</div>
	<div class="cart-act"><a class="button cart-bag" href="#">Giỏ hàng</a><a class="button cart-bag" href="#">Thanh Toán</a></div>
	<script type="text/javascript">
		$('.remove-cart').click(function(event) {
			var product_id =$(this).attr("the-id");
			$.ajax({
				type: 'POST',
				data: {
				    action: 'remove-cart-item',
				    cartid : product_id,
				    nonce: WPAjax.nonce,
				},
				url: WPAjax.url,
				beforeSend: function() {

				},
				success: function(data) {
				    alert("Xóa khỏi giỏ hàng thành công");
				    // load_cart_again = true;
				    load_cart();
				    // window.location = window.location;
				}
			});
		});
	</script>