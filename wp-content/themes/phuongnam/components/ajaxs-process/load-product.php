<?php 
$id_product = $_POST['productID'];
$_post = get_post( $id_product );
$product = wc_get_product( $id_product );
$attachment_ids = $product->get_gallery_attachment_ids();
$list_imgs;
foreach ($attachment_ids as $key => $value) {
	$list_imgs[$value]['url'] = wp_get_attachment_url($value);
	$list_imgs[$value]['id'] = 'id'.$value;
}
// echo '<pre>'.__FILE__.'::'.__METHOD__.'('.__LINE__.')<br>'; 
// 	print_r($list_imgs); 
// echo '</pre>';
?>
<div class="view-product-popup view-product-popup-wrap modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-5 col-md-6">
						<div class="image-zoom">
							<div class="owl-carousel owl-theme">
								<?php foreach ($list_imgs as $key => $value): ?>
								<div class="item" data-hash="<?php echo $value['id'] ?>"><img src="<?php echo $value['url'] ?>" alt=""/></div>
								<?php endforeach ?>
							</div>
						</div>
						<div class="nav-thuml">
							<div class="owl-carousel owl-theme">
								<?php foreach ($list_imgs as $key => $value): ?>
									<div class="item"><a href="#<?php echo $value['id'] ?>"><img src="<?php echo $value['url'] ?>" alt=""/></a></div>
								<?php endforeach ?>
							</div>
						</div>
					</div>
					<div class="col-lg-7 col-md-6">
						<div class="modal-title">
							<h4><?php echo $_post->post_title ?></h4>
						</div>
						<div class="modal-price">
							<h3><?php echo wc_price($product->get_price()); ?></h3>
						</div>
						<form id="cart-number" action="">
							<div class="number-input">
								<label>Số lượng</label>
								<input type="number" min="1" value="1" class="the-number" />
							</div>
							<div class="add-input">
								<button id="add-to-cart-popup" class="add-to-cartx" the-id="<?php echo $_post->ID ?>" >
									 Thêm vào giỏ</button><span>hoặc </span><a href="<?php echo get_permalink( $_post->ID ) ?>">Xem chi tiết</a>
							</div>
						</form>
					</div>
					<script type="text/javascript">
						jQuery(document).ready(function($) {
							$('.add-to-cartx').click(function(event) {
								event.preventDefault();
								var id = $(this).attr('the-id');
								var number = $('.the-number').val();
								$.ajax({
									type: 'POST',
									data: {
										action: 'add-to-cart',
										productID : id,
										number : number,
										nonce: WPAjax.nonce,
									},
									url: WPAjax.url,
									beforeSend: function() {

									},
									success: function(data) {
									    alert("Thêm vào giỏ hàng thành công!");
									    load_cart();
									}
								});
							});
						})
					</script>
				</div>
			</div>
		</div>
	</div>
</div>


