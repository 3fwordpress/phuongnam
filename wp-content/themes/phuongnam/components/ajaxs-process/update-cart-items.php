<?php

$cart = WC()->cart;

$obj_update = $_POST['obj_update'];
$arr = json_decode( $obj_update,false );

foreach ($obj_update as $key => $value) {
	// echo '<pre>'.__FILE__ .'::'.__METHOD__ .'('.__LINE__ .')<br>';
	// 	print_r($obj_update);
	// echo '</pre>';
	$cart->set_quantity( $value['element']['key'] , $value['element']['quantity']);
}
?>
	<thead>
		<tr>
			<th class="image">&nbsp;</th>
			<th class="item">Tên sản phẩm</th>
			<th class="qty">Số lượng</th>
			<th class="price">Giá tiền</th>
			<th class="remove">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php
	global $woocommerce;
	$items = $woocommerce->cart->get_cart();
	$tong = 0;
	foreach($items as $item => $values) { 
		$_product =  wc_get_product( $values['data']->get_id()); 
		$price = get_post_meta($values['product_id'] , '_price', true);
		$tong += ($price*$values['quantity']);
		$img_avt = get_the_post_thumbnail_url( $values['product_id'] );
		$img_avt = !$img_avt?(TFT_URL.'/no_photo.jpg'):$img_avt;
		?>
		<tr class="active">
			<td class="image"><img src="<?php echo $img_avt ?>" alt=""/></td>
			<td class="item"> <strong><a href="<?php echo get_permalink( $_product->get_ID() ) ?>"><?php echo $_product->get_title() ?></a></strong></td>
			<td class="qty"> 
				<input type="number" value="<?php echo $values['quantity'] ?>" min="1"/>
			</td>
			<td class="price"> <span><?php echo wc_price($price*$values['quantity']) ?></span></td>
			<td class="remove"> <a class="remove" id="<?php echo $values['key'] ?>" href="#">xóa</a></td>
		</tr>
		<?php
	}
	?>
	</tbody>
	<tfoot style="font-family: 'arial';">
		<tr class="show-table">
			<td class="image"></td>
			<td class="item"> <a href="#"></a></td>
			<td class="qty"> <strong>Tổng cộng</strong></td>
			<td class="price"> <span><?php echo wc_price($tong) ?></span></td>
			<td class="remove"></td>
		</tr>
	</tfoot>