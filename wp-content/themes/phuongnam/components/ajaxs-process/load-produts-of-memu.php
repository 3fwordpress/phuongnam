<?php 

	$args = array(
		'posts_per_page' => -1,
		'post_type' => 'product',
		'tax_query' => array(
			array(
				'taxonomy' => 'thuc-don',
				'field' => 'term_id',
				'terms' => $_POST['productID'] ,
			)
		)
	);
	$_posts = get_posts( $args ); 
	// echo '<pre>'.__FILE__.'::'.__METHOD__.'('.__LINE__.')<br>'; 
	// 	print_r($_posts); 
	// echo '</pre>';
	?>
	<?php foreach ($_posts as $key => $value): ?>
		<?php $product = wc_get_product( $value->ID ); ?>
		<div class="col-sm-4 col-xs-6">
			<div class="product product-wrap component">
				<div class="product-info"><a href="#">
						<div class="product-info__img"><img src="<?php echo get_the_post_thumbnail_url( $value->ID ) ?>" alt=""/></div>
						<div class="product-info__act">
							<ul class="list-inline">
								<li class="add-cart">
									<a class="button add_to_cart" data-toggle="modal" the-id="<?php echo $value->ID ?>" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a>
								</li>
								<li class="info">
									<a class="img" href="<?php echo get_permalink( $value->ID  ) ?>"></a>
								</li>
								<li class="view">
									<span class="img view-popupx" data-toggle="modal" data-target="#view-product-popup" the-id="<?php echo $value->ID ?>"></span>
								</li>
							</ul>
						</div></a></div>
				<div class="product-box">
					<div class="product-box__name fleft"><a href="#"><?php echo $product->get_title() ?></a></div>
					<div class="product-box__price fright">
						<h4><?php echo wc_price($product->get_price()); ?></h4>
					</div>
					<div class="clear-fix"></div>
				</div>
			</div>
			<!--.product-wrap-->
		</div>
		<script type="text/javascript">
			$(".view-popupx").click(function(event) {
				// console.log($(this).attr('the-id'));
				var the_id = $(this).attr('the-id');
				$.ajax({
					type: 'POST',
					data: {
						action: 'load-product',
						productID: the_id,
						nonce: WPAjax.nonce
					},
					url: WPAjax.url,
					beforeSend: function() {
						var xxx = '<div class="view-product-popup view-product-popup-wrap modal-dialog"><div style="width: 100%;text-align: center;margin: 200px 0px;"><div class="modal-content"><div class="modal-body"><i style="font-size: 100px;color:black" class="fa fa-refresh fa-spin fa-3x fa-fw" aria-hidden="true"></i></div></div></div></div>';
						$('#view-product-popup').html(xxx);
					},
					success: function(data) {
						$('#view-product-popup').html(data);
						// alert("Mở popup thành công");
						$(".image-zoom .owl-carousel").owlCarousel({
					        loop: !0,
					        animateOut: "fadeOut",
					        animateIn: "fadaIn",
					        margin: 10,
					        nav: !1,
					        dots: !1,
					        responsive: {
					            0: {
					                items: 1
					            },
					            600: {
					                items: 1
					            },
					            1e3: {
					                items: 1
					            }
					        }
					    });
					    $(".nav-thuml .owl-carousel").owlCarousel({
					        loop: !1,
					        margin: 10,
					        nav: !0,
					        dots: !1,
					        center: !0,
					        mouseDrag: !1,
					        navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
					        responsive: {
					            0: {
					                items: 4
					            },
					            600: {
					                items: 4
					            },
					            1e3: {
					                items: 4
					            }
					        }
					    });
					},
					error: function(data) {
						console.log(data);
					}
				});
			});
		</script>
	<?php endforeach ?>
