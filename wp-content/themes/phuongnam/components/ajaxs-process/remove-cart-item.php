<?php
/**
 * Remove cart line and Update the cart contents
 */
$cart = WC()->cart;
$cart->remove_cart_item($_POST['cartid']);

wp_send_json_success( array(
    'cartTotalsItem' => $cart->cart_contents_count,
    'cartTotalsPrice' => $cart->get_total(),
) );;