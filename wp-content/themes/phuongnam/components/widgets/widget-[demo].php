<?php
/**
 * Widget for slide categories.
 *
 * @package 3F Web
 * @subpackage twtheme
 * @since 1.0.0
 */

add_action( 'widgets_init', function() {
    register_widget( 'TF_Demo_Widget' );
});

class TF_Demo_Widget extends TF_Widgets {

    //
    public function widget_defind() {
        $widget_defind = array( 
            'widget_id' => 'twtheme_demo_widget',
            'widget_name' => __( '3F: Demo Widget', 'twtheme' ),
            'description' => __( 'Hiển thị phần thông tin cuối trang.', 'twtheme' )
        );
        return $widget_defind;
    }

    // the method defind widget fields
    public function widget_fields() {

        //get all categories
        $tw_category_dropdown = tw_category_dropdown();

        //get all pages
        $tw_pages_dropdown = tw_pages_dropdown();
        unset($tw_pages_dropdown[0]);

        //defind widget fields 
        $fields = array(
            'tw_dropdown' => array(
                'tw_widgets_name'         => 'tw_dropdown',
                'tw_widgets_title'        => __( 'Chuyên mục chứa dịch vụ', 'twtheme' ),
                'tw_widgets_field_type'   => 'select',
                'tw_widgets_default'      => 0,
                'tw_widgets_field_options'=> $tw_category_dropdown
            ),
            'tw_multicheckboxes' => array(
                'tw_widgets_name'         => 'tw_multicheckboxes',
                'tw_widgets_title'        => __( 'Chọn một trang', 'twtheme' ),
                'tw_widgets_field_type'   => 'multicheckboxes',
                'tw_widgets_default'      => 0,
                'tw_widgets_field_options'=> $tw_pages_dropdown
            ),
            'tw_text' => array(
                'tw_widgets_name'         => 'tw_text',
                'tw_widgets_title'        => __( 'Nhập vào tiêu đề', 'twtheme' ),
                'tw_widgets_field_type'   => 'text',
                'tw_widgets_default'      => ''              
            ),
            'tw_number' => array(
                'tw_widgets_name'         => 'tw_number',
                'tw_widgets_title'        => __( 'Nhập vào một số', 'twtheme' ),
                'tw_widgets_field_type'   => 'number',
                'tw_widgets_default'      => 0         
            ),
            'tw_texarea' => array(
                'tw_widgets_name'         => 'tw_texarea',
                'tw_widgets_title'        => __( 'Nhập vào nội dung', 'twtheme' ),
                'tw_widgets_field_type'   => 'textarea',
                'tw_widgets_default'      => '' ,
                'tw_widgets_row' => 4
            ),
            'tw_upload_2' => array(
                'tw_widgets_name'         => 'tw_upload_2',
                'tw_widgets_title'        => __( 'Chọn một ảnh thu 2', 'twtheme' ),
                'tw_widgets_field_type'   => 'upload',
                'tw_widgets_default'      => '' ,
                'tw_widgets_row' => 4
            ),
        );

        return $fields;
    }


    public function widget_show( $args, $data ) {

        extract( $args );
        if( empty( $data ) ) {
            return ;
        }

        echo '<pre>';
        print_r($data);
        
        echo '</pre>';

    }

}
