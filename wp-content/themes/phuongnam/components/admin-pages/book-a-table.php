<?php 
/**
 * Title: Đặt bàn
 * Name: Đặt bàn
 * Order: 20
 * capability: manage_options
 * Icon: dashicons-editor-code
 */
$data =get_option( 'booked');
?>
<div class="wrap">
	<table class="wp-list-table widefat fixed striped pages" style="width: 101%">
		<thead>
			<tr>
				<td style="width: 4%">
					STT
				</td>
				<td style="width: 12%">
					Họ và Tên
				</td>
				<td style="width: 10%">
					Số ĐT
				</td>
				<td style="width: 12%">
					Thời gian tổ chức
				</td>
				<td style="width: 7%">
					Số người
				</td>
				<td>
					Danh sách các món ăn
				</td>
				<td style="width: 25%">
					Ý kiến khách hàng
				</td>
				<td style="width: 12%">
					Thời gian đặt
				</td>
				<td style="width: 4%">
					<input type="checkbox" id="check_all" name="">
				</td>
			</tr>
		</thead>
		<tbody>
			<?php 
			$stt =1;
			foreach ( (array) $data as $key => $value) {
			$post = get_post( $value['id_post'] );
			?>
				<tr id="key-<?php echo $key ?>">
					<td hidden ><input type="" name="id" id="id" value="<?php echo $key ?>"></td>
					<td><?php echo $stt ?></td>
					<td><?php echo $value['name'] ?></td>
					<td><?php echo $value['number_phone'] ?></td>
					<td><?php echo $value['date_order'].' - '.$value['time_order'] ?></td>
					<td><?php echo $value['people'] ?></td>	
					<td><?php echo get_the_title() ?>
						<?php $_count = 1; ?>
						<?php foreach ($value['arr_mon'] as $_id_post): ?>
							<a href="<?php echo get_permalink($_id_post) ?>"><?php echo get_the_title( $_id_post ) ?></a>
							<?php echo $_count==count($value['arr_mon'])?'':'<br>' ?>
							<?php $_count++; ?>
						<?php endforeach ?>
					</td>
					<td>
						<textarea rows="5" readonly="" style="width: 100%;resize: none;">
							<?php echo $value['message'] ?>
						</textarea>
					</td>	
					<td><?php echo date( "Y-m-d H:i", ($key +((7)*3600)) )   ?></td>
					<td><input style="margin-left: 8px;" type="checkbox" id="<?php echo $key ?>" class="checkxxx" name=""></td>
				</tr>
			<?php	
			$stt++;
			} ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="9">
					<button class="button button-primary button-large btn_delete" style="float: right;" >Xóa những thông tin đã chọn</button>
					<div style="clear: both;"></div>
				</td>
			</tr>
		</tfoot>
		<script type="text/javascript">
				jQuery(document).ready(function($) {
					$('.btn_delete').click(function(event) {
						event.preventDefault();
						//var keyx=$(this).attr('id');
						var val = [];
						$('tbody :checkbox:checked').each(function(i){
							val[i] = $(this).attr('id');
						});
						console.log(val);
						console.log( JSON.stringify(val) );
						var res =JSON.stringify(val);

						var vld=confirm("Bạn chắc chắn muốn xóa chứ!");
						if (vld == true ) {
							$.ajax({
								url: '<?php echo admin_url( 'admin-ajax.php' ) ?>',
								type: 'POST',
								dataType: 'json',
								data: {
									key: val,
									action: 'ajax_delete_booked'
								 },
								success: function(data) {
									val.forEach(function(element) {
										$('#key-'+element).remove();
									});
									
									console.log(data);
								},
								error:function(data) {
									console.log("Thất bại: "+data);
								}
							});
						}
					});

					$('#check_all').click(function(event) {
						if (!check_all) {
							$( "tbody tr td .checkxxx" ).prop( "checked", false );
						}
						else{
							$( "tbody tr td .checkxxx" ).prop( "checked", true );
						}
						check_all=(!check_all);
					});
				});
			</script>
	</table>
</div>