<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

//do_action( 'woocommerce_before_checkout_form', $checkout );
// echo '<pre>'.__FILE__.'::'.__METHOD__.'('.__LINE__.')<br>'; 
// 	print_r($checkout); 
// echo '</pre>';
// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

?>

<?php 
$checkout = WC()->checkout();
?>
<div style="display: none;"></div>
<div id="wrapper">
	<div class="checkout-page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 text-center"><a href="<?php echo home_url( 'gio-hang') ?>">
						<h1 class="checkout-title"><span class="back">Quay về giỏ hàng</span>Nhà hàng phương nam</h1></a></div>
				<div class="col-xs-12 form-user">
					<!-- <form class="row" id="user-form" action=""> -->
					<form class="checkout_coupon woocommerce-form-coupon" method="post">
						<p><?php esc_html_e( 'If you have a coupon code, please apply it below.', 'woocommerce' ); ?></p>

						<p class="form-row form-row-first">
							<input type="text" name="coupon_code" class="input-text" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" id="coupon_code" value="" />
						</p>

						<p class="form-row form-row-last">
							<button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_html_e( 'Apply coupon', 'woocommerce' ); ?></button>
						</p>

						<div class="clear"></div>
					</form>
					<form name="checkout" method="post" class="checkout woocommerce-checkout row" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
						<div class="col-md-4">
							<div class="info">
								<h2>Thông tin giao hàng</h2>
								<?php if (!is_user_logged_in()): ?>
									<div class="user-login"><a href="<?php echo home_url( 'register' ) ?>">Đăng kí tài khoản mua hàng</a><span>|</span><a href="<?php echo home_url( 'log-in' ) ?>">Đăng nhập</a></div>
								<?php endif ?>
								<hr/>
								<div class="form-info">
									<?php if (!is_user_logged_in()): ?>
										<label>Mua không cần tài khoản</label>
									<?php endif ?>
									<?php do_action( 'woocommerce_checkout_billing' ); ?>
									<?php //do_action( 'woocommerce_checkout_shipping' ); ?>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="shipping" >
								<h3>Thanh toán</h3>
								
									<?php 
									$_payments=WC()->payment_gateways->payment_gateways();
									$available_gateways = WC()->payment_gateways->get_available_payment_gateways();
									// echo '<pre>'.__FILE__.'::'.__METHOD__.'('.__LINE__.')<br>'; 
									// 	print_r($available_gateways); 
									// echo '</pre>';
									?>
									<?php if ( WC()->cart->needs_payment() ) : ?>
										<div class="how-pay  payment_methods methods" id="customer_details">
											<?php
											if ( ! empty( $available_gateways ) ) {
												foreach ( $available_gateways as $gateway ) {
													wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
												}
											} else {
												echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) : esc_html__( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</li>'; // @codingStandardsIgnoreLine
											}
											?>
										</div>
									<?php endif; ?>
								<?php wc_print_notices(); ?>
							</div>
						</div>
						<div class="col-md-4">
							<div class="box-cart">
								<h2>Đơn hàng<span>(</span><span>1</span><span>sản phẩm)</span></h2>
								<div class="cart-items">
									<?php 
									global $woocommerce;
									$items = $woocommerce->cart->get_cart();
									$tong = 0;
									foreach($items as $item => $values) { 
										$_product =  wc_get_product( $values['data']->get_id()); 
										$price = get_post_meta($values['product_id'] , '_price', true);
										$tong += ($price*$values['quantity']);
										$img_avt = get_the_post_thumbnail_url( $values['product_id'] );
										$img_avt = !$img_avt?(TFT_URL.'/no_photo.jpg'):$img_avt;
										?>
										<div class="list-item"><span>1 x </span><span><?php echo $_product->get_name(); ?></span><span class="price fright"><?php echo wc_price($price*$values['quantity']) ?></span>
											<div class="clear-fix"></div>
										</div>

										<?php 
										$shipping_class_id = $_product->get_shipping_class_id();
										$shipping_class= $_product->get_shipping_class();
										$fee = 0;

										if ($shipping_class_id) {
										   $flat_rates = get_option("woocommerce_flat_rates");
										   $fee = $flat_rates[$shipping_class]['cost'];
										}

										$flat_rate_settings = get_option("woocommerce_flat_rate_settings");
										// echo 'Shipping cost: ' );
										?>
									<?php } ?>
								</div>
								<div class="total-price">
									<p class="fleft">Tạm tính</p>
									<p class="fright"> <span><?php echo wc_price($tong) ?></span></p>
									<div class="clear-fix"></div>
								</div>
								<div class="shiping-price">
									<p class="fleft">Phí vận chuyển</p>
									<div class="fright" style="float: right;">
										<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
											<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>
											<?php wc_cart_totals_shipping_html(); ?>
											<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>
										<?php endif; ?>
									</div>
									<div class="clear-fix"></div>
								</div><a class="btn-coupon"><span class="click-show">Sử dụng mã giảm giá<span> <i class="fas fa-chevron-right"></i></span></span></a>
								<div class="coupon">
									<input type="text" id="ahihi_input" placeholder="Sử dụng mã giảm giá"/><a href="#" id="ahihi_click">Sử dụng</a>
									<div class="clear-fix"></div>
								</div>
								<div class="total-checkout">
									<p class="fleft">Tổng cộng</p>
									<p class="fright"><span><?php echo wc_price($tong+$flat_rate_settings['cost_per_order'] + $fee) ?></span></p>
									<div class="clear-fix"></div>
								</div>
							</div>
							<div class="form-row place-order">
								<noscript>
									<?php esc_html_e( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ); ?>
									<br/><button type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>"><?php esc_html_e( 'Update totals', 'woocommerce' ); ?></button>
								</noscript>
								<?php
									do_action( 'woocommerce_review_order_before_submit' ); 
									$order_button_text = "Đặt hàng"
								?>
								<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="<?php echo esc_html( $order_button_text ) ?>" data-value="<?php echo esc_html( $order_button_text ) ?>"><?php echo esc_html( $order_button_text ) ?></button>
								<?php do_action( 'woocommerce_review_order_after_submit' ); ?>
							</div>
						</div>
						<?php do_action( 'woocommerce_review_order_after_submit' ); ?>

						<?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
						<div class="clear-fix"></div>
					</form>
					<?php wc_get_template( 'checkout/terms.php' ); ?>

				</div>
			</div>
		</div>
	</div>
</div>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
