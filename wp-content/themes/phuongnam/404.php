<?php 
/**
 * 404 not found file, the file called if a link not correct
 * @author 3F Wordpress Team
 * @link http://3fgroup.vn
 */
?>

<?php get_header(); ?>

<h3><?php _e('Nội dung bạn vừa yêu cầu không được tìm thấy.') ?></h3>

<?php get_footer(); ?>
