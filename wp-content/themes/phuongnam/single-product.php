<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
session_start();
get_header( 'shop' ); ?>
<?php
	if (!isset($_SESSION['id-'.$post->ID])) {
		$_SESSION['id-'.$post->ID] = time();
		add_post_meta( $post->ID, 'count_view',1 );
	}else{
		count_view($post->ID,$_SESSION['id-'.$post->ID]);
	}?>
	<div class="products-page">
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<ol class="breadcrumb breadcrumb-arrow hidden-sm hidden-xs">
							<li><a href="#">Trang chủ</a></li>
							<li><a href="#">Thực đơn chọn món</a></li>
							<li class="active"><span> Chè bà ba</span></li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<div class="main-product">
			<div class="container">
				<?php while ( have_posts() ) : the_post(); ?>

					<?php 
						$product = wc_get_product( get_the_ID() );
						$attachment_ids = $product->get_gallery_attachment_ids();
						$list_imgs;
						foreach ($attachment_ids as $key => $value) {
							$list_imgs[$value] = wp_get_attachment_url($value);
						}
					?>

					

				<div class="row">
					<div class="col-md-5">
						<div class="slider-pc">
							<div class="flexslider" id="slider">
								<ul class="slides">
									<?php foreach ($list_imgs as $key => $value): ?>
										<li><img class="zoom" src="<?php echo $value ?>" data-zoom-image="<?php echo $value ?>"/></li>	
									<?php endforeach ?>
								</ul>
							</div>
							<div class="flexslider" id="carousel">
								<ul class="slides">
									<?php foreach ($list_imgs as $key => $value): ?>
										<li><img src="<?php echo $value ?>"/></li>	
									<?php endforeach ?>
								</ul>
							</div>
						</div>
						<div class="slider-mobie">
							<div class="image-zoom">
								<div class="owl-carousel owl-theme">
									<?php foreach ($list_imgs as $key => $value): ?>
										<!-- <li><img src="<?php echo $value ?>"/></li>	 -->
										<div class="item" data-hash="img_<?php echo $key ?>"><img src="<?php echo $value ?>" alt=""/></div>
									<?php endforeach ?>
								</div>
							</div>
							<div class="nav-thuml">
								<div class="owl-carousel owl-theme">
									<?php foreach ($list_imgs as $key => $value): ?>
										<!-- <li><img src="<?php echo $value ?>"/></li>	 -->
										<div class="item"><a href="#img_<?php echo $key ?>"><img src="<?php echo $value ?>" alt=""/></a></div>
									<?php endforeach ?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="product-title">
							<h1><?php the_title(  ) ?></h1>
						</div>
						<div class="product-price"><span><?php echo  wc_price($product->get_price()); ?> <sup>
								</div>
						<p>
							<?php the_excerpt(); ?>
						</p>
						<form class="row" id="add-item-product" action="">
							<div class="quantity-box col-lg-3 col-xs-5"><span class="quantity">
									<input class="quantity-val" type="text" min="1" value="1" name="quantity"/>
									<input type="text" style="display: none;" class="product-id" value="<?php echo get_the_ID() ?>" >
									<div class="quantity-up">
										<button type="button">+</button>
									</div>
									<div class="quantity-down">
										<button type="button">-</button>
									</div>
									<div class="clear-fix"></div></span></div>
							<div class="clear-fix"></div>
							<div class="row col-lg-9">
								<div class="col-sm-6">
									<button class="btn-detail btn-add" id="add-cart" type="button">Thêm vào giỏ</button>
								</div>
								<div class="col-sm-6">
									<button class="btn-detail btn-buy" id="buy-cart" type="submit">Mua ngay</button>
								</div>
							</div>
						</form>
						<script type="text/javascript">
							jQuery(document).ready(function($) {
								$('#add-cart').click(function(event) {
									event.preventDefault();
									var number_item = $('.quantity-val').val();
									var product_id = $('.product-id').val();
									$.ajax({
									    type: 'POST',
									    data: {
									        action: 'add-to-cart',
									        productID : product_id,
											number : number_item,
									        nonce: WPAjax.nonce,
									    },
									    url: WPAjax.url,
									    beforeSend: function() {

									    },
									    success: function(data) {
									    	load_cart();
									        console.log(data);
									        alert("Thêm vào giỏ hàng thành công");
									    }
									});
								})
							});
						</script>
					</div>
				</div>
				<div class="row">
						<div class="col-lg-3">
						<ul class="nav nav-tabs" id="page-product">
							<li class="active"><a href="#mota" aria-controls="mota" data-toggle="tab" role="tab">Mô tả sản phẩm</a></li>
							<li><a href="#binhluan" aria-controls="binhluan" data-toggle="tab" role="tab">Bình luận</a></li>
						</ul>
					</div>
					<div class="col-lg-9">
						<div class="tab-content">
							<div class="tab-pane active" id="mota" role="tabpanel">
								<div class="container-fluid product-description-wrapper">
									<p>
										<?php the_content( ) ?>
									</p>
								</div>
							</div>
							<div class="tab-pane" id="binhluan" role="tabpanel">
								<div class="container-fluid">
									<div class="row">
										<div id="fb-root"></div>
										<script>(function(d, s, id) {
										var js, fjs = d.getElementsByTagName(s)[0];
											if (d.getElementById(id)) return;
											js = d.createElement(s); js.id = id;
											js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=/*my app ID*/";
											fjs.parentNode.insertBefore(js, fjs);
										}(document, 'script', 'facebook-jssdk'));
										</script>
										<div class="fb-comments" data-href="<?php echo get_permalink( get_the_ID() ) ?>" data-num-posts="10" data-width="100%"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php endwhile; // end of the loop. ?>

				<div class="related">
					<div class="related__title">
						<h3>Sản phẩm liên quan</h3>
					</div>
					<hr/>
					<?php 
						// echo '<pre>'.__FILE__ .'::'.__METHOD__ .'('.__LINE__ .')<br>';
						// 	print_r(get_the_ID());
						// echo '</pre>';
						$_terms = get_the_terms( get_the_ID(), 'product_cat' );
						// echo '<pre>'.__FILE__ .'::'.__METHOD__ .'('.__LINE__ .')<br>';
						// 	print_r($_terms);
						// echo '</pre>';
					?>
					<div class="row related__products">
						<?php $countx =1; 
						?>
						<?php foreach ($_terms as $key => $value){

							if ($countx==5) {
								break;
							}
							$_id_term = $value->term_id;
							$args = array(
								'post_type' => 'product',
								'tax_query' => array(
									array(
										'taxonomy' => 'product_cat',
										'field' => 'term_id',
										'terms' => $_id_term
									)
								)
							);
							$_posts = get_posts( $args  );
							foreach ($_posts as $key_2 => $value_2) {
								if ($countx==5) {
									break;
								}
								$_product = wc_get_product( $value_2->ID );
								$img_avt = get_the_post_thumbnail_url( $value_2->ID );
								$img_avt = !$img_avt?(TFT_URL.'/no_photo.jpg'):$img_avt;
								?>
								<div class="col-md-3 col-sm-6">
									<div class="related-products related-products-wrap"><a href="<?php echo get_permalink( $value_2->ID ) ?>">
											<div class="related-products__img"><img src="<?php echo $img_avt; ?>" alt=""/></div>
											<div class="related-products__price">
												<p><?php echo  wc_price($_product->get_price()); ?><span> <sup>
											</div>
											<div class="related-products__name">
												<h3><?php echo $value_2->post_title ?></h3>
											</div>
											<div class="related-products__made">
												<p>	by nhà hàng Phương Nam</p>
											</div></a>
									</div>
								</div>
								<?php
							$countx++; }
						} ?>
					</div>
				</div>
			</div>
		</div>
	</div>



	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		//do_action( 'woocommerce_before_main_content' );
	?>

		
	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		//do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
