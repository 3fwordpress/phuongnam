======CHANGE LOG v1.3.0======
- Tích hợp license, nếu license k chính xác, toàn bộ chức năng của framework sẽ bị dùng.
  + cập nhật license tại <admin_url>/admin.php?page=framework-options
- Tích hợp bootstrap vào các custom admin page
- Thay đổi giao diện của Framework Options
- Fix một số thứ

======CHANGE LOG v1.2.9======
- Tạo admin page dễ dàng hơn bao giờ hết. Sử dụng class WP_Pages trong file components.php vẫn được nhé =))
 + Tạo file *.php mới tại thư mục components/admin-pages
 + Trong file vừa tạo, comment block thông tin của page (xem file test-page[demo].php).
      Trong đó:
        Name - Tên menu
        Title - Tiêu đề page
        Order - Thứ tự của menu
        Icon - Dash icon cho menu
        Parent - slug của menu cha (để "null" hoặc xóa dòng này nếu page không có page cha)

- Tạo ajax dễ dàng hơn: Chỉ việc tạo file *.php vào folder components/ajaxs-process
  + Tên file (không gồm .php) là action của ajax
  + Code js sẽ để ỏ một file .js nào đó trong thư mục public hoặc bất kỳ đâu trong site
  + Xem file demo trong folder để biết thêm

- Require thêm plugin Easy WP SMTP

- Tích hợp Google API Key cho acf Map. 
 + Sử dụng hàm acf_get_google_map(get_field( <Map key> )) tại chỗ cần hiển thị

- Thêm folder Rest API

=======CHANGE LOG v1.2.7======
- Các file trong thư mục incfiles, functions, hooks sẽ đc tự động include