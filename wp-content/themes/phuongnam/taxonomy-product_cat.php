<?php 
/*
* author: Nguyen Trong Giap
*/
?>
<?php get_header( ) ;
?>
<section class="set-menu" id="set-menu">
	<div class="topbar topbar-wrap">
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<ul class="list-inline">
							<?php //path_from_province_archive(); ?>
							<li> <a href="<?php echo home_url() ?>">Trang chủ</a></li>
							<li class="li-display"><a href="#">Danh mục</a></li>
							<li><a href="#">Set Menu tại nhà hàng Phương Nam</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php 
	global $wp;

	$link = home_url( $wp->request );
	$_term = get_queried_object();
	$order = 'ASC';
	$orderby = 'meta_value';
	$tax_query = '';
	if(!empty($_GET['sort'])){
		if ($_GET['sort']=='down'||$_GET['sort']=='za') {
			$order = 'DESC';
		}
		if($_GET['sort']=='sp'){
			$orderby = 'date';
		}
		if($_GET['sort']=='az'||$_GET['sort']=='za'){
			$orderby = 'title';
		}
		if($_GET['sort']=='up'||$_GET['sort']=='down'){
			$meta_key = '_price';
			$orderby ='meta_value';
		}
		
	}
	$args = array(
		'posts_per_page'=> 1,
		'post_type'		=> 'product',
		'orderby'		=> $orderby,
		'order'			=> $order,
		'meta_key'		=> $meta_key,
		// 'meta_type'		=> $meta_type,
		'paged' => isset($_GET['pg'])?$_GET['pg']:1,
		'tax_query'		=> array(
			array(
				'taxonomy' => 'product_cat',
				'field' => 'term_id',
				'terms' => $_term->term_id
			)
		)
		// 'meta_query' => $meta_query

	);
	$_posts = new WP_Query( $args );
	?>
	<div class="goodsale-content">
		<div class="container">
			<div class="row">
				<div class="main-content col-sm-12">
					<div class="row">
						<div class="col-lg-3 col-sm-12 col-3">
							<aside class="sidebar listmenu1">
								<?php do_action( 'archive_product_left_hook' ) ?>
							</aside>
						</div>
						<div class="col-lg-9 col-sm-12 col-9">
							<div class="row">
								<div class="col9-content">
									<div class="title">
										<h4>Set Menu</h4>
									</div>
									<div class="toolbar">
										<div class="row">
											<div class="col-lg-12 col-sm-12">
												<div class="toolbar-box fleft">
													<ul class="list-inline fleft">
														<li><a class="large" href="#"><i class="fa fa-th-large" aria-hidden="true"></i></a></li>
														<li><a class="list" href="#"><i class="fa fa-list-ul" aria-hidden="true"></i></a></li>
													</ul>
													<div class="sort-tag fleft"><span>Sắp xếp theo:</span>
														<form method="get_header" action="" style="display: inline;">
															<select name="sort" id="select_sort">
																<option value="sp" <?php echo $_GET['sort']=='sp'?'selected':''; ?>>Sản phẩm nổi bật</option>
																<option value="up" <?php echo $_GET['sort']=='up'?'selected':''; ?>>Giá: Tăng dần</option>
																<option value="down"<?php echo $_GET['sort']=='down'?'selected':''; ?>>Giá: Giảm dần</option>
																<option value="az" <?php echo $_GET['sort']=='az'?'selected':''; ?>>Tên: A - Z</option>
																<option value="za" <?php echo $_GET['sort']=='za'?'selected':''; ?>>Tên: Z - A</option>
															</select>
															<button type="submit" style="display: none;" id="submit_search">Tìm kiếm</button>
															<script type="text/javascript">
																$(function() {
																	$('#select_sort').change(function(event) {
																		console.log($(this).find(":selected").val());
																		$('#submit_search').click();
																	});
																});
															</script>
														</form>
													</div>
													<div class="clear-fix"></div>
												</div>
												<div class="clear-fix"></div>
											</div>
										</div>
									</div>
									<div class="menu-content">
										<div class="row">
										<?php while ($_posts->have_posts()) {
											$_posts->the_post();
											//foreach ($_posts as $key => $value){
											$_img = (get_the_post_thumbnail_url( $post, 'full' ));
											$_img = empty($_img)?(TFT_URL.'/default.jpg'):$_img;
											$product = wc_get_product( $post->ID );
											
											 ?>
											<div class="col-sm-4 col-xs-6">
												<div class="product product-wrap component">
													<div class="product-info"><a href="#">
														<div class="product-info__img"><img src="<?php echo $_img ?>" alt=""/></div>
														<div class="product-info__act">
															<ul class="list-inline">
																<li class="add-cart"><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																<li class="info"><a class="img" href="#"></a></li>
																<li class="view"><span class="img" data-toggle="modal" data-target="#view-product-popup" id="view-popup"></span></li>
															</ul>
														</div></a></div>
													<div class="product-box">
														<div class="product-box__name fleft"><a href="#"><?php the_title() ?></a></div>
														<div class="product-box__price fright">
															<h4><?php echo wc_price($product->get_price()); ?></h4>
														</div>
														<div class="clear-fix"></div>
													</div>
												</div>
												<!--.product-wrap-->
											</div>
										<?php } ?>
										</div>
									</div>
									<div class="menu-content2">
										<div class="row">
											<div class="col-sm-12">
											<?php while ($_posts->have_posts()) {
												$_posts->the_post();
												$_img = (get_the_post_thumbnail_url( $post, 'full' ));
												$_img = empty($_img)?(TFT_URL.'/default.jpg'):$_img;
												$product = wc_get_product( $post->ID );
												?>
												<div class="list-product list-product-wrap">
													<div class="list-wrap">
														<div class="row">
															<div class="col-sm-5"><a class="fleft" href="#">
																<div class="product-info__img"><img src="<?php echo $_img ?>" alt=""/>
																	<div class="layer-prd"><i class="fa fa-search" aria-hidden="true"></i></div>
																</div></a></div>
															<div class="col-sm-7">
																<div class="list-prd__infor fleft"><a href="#">
																		<h3><?php the_title() ?></h3></a>
																	<h4><?php echo wc_price($product->get_price()); ?></h4>
																	<p><?php echo wp_trim_words( get_the_content(), 15, '...' ) ?></p>
																	<ul class="list-inline">
																		<li><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																		<li><a class="img" href="#"></a></li>
																		<li><span class="img" data-toggle="modal" data-target="#view-product-popup"></span></li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
											<?php } ?>
											</div>
										</div>
									</div>
									<div class="Pagination">
										<div class="row">
											<?php mt_destination_navigate(false,$_posts,$link); ?>
											<!-- <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a class="prev" href="#"><span><i class="fa fa-angle-left"></i>&nbsp;Trang trước</span></a></div>
											<div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
												<ul class="list-inline">
													<li><a class="a-active" href="#">1</a></li>
													<li><a href="#">2</a></li>
													<li><a href="#">3</a></li>
													<li><a href="#">4</a></li>
													<li class="dots"><a href="#">...</a></li>
													<li><a href="#">10</a></li>
												</ul>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 text-right"><a class="next" href="#"><span>Trang sau&nbsp;<i class="fa fa-angle-right"></i></span></a></div> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="up up-active"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
</section>
	<div class="modal fade" id="add-cart-popup" role="dialog">
		<div class="add-cart-popup add-cart-popup-wrap modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
					<div class="row">
						<div class="col-sm-6">
							<div class="cart-modal">
								<h2 class="modal-title"> <i class="fas fa-check"></i><span>Sản phẩm đã được thêm vào giỏ</span></h2>
								<div class="product-info">
									<div class="product-info__img fleft"><img src="<?php echo TFT_URL; ?>/public/images/ta_product.jpg" alt=""/></div>
									<div class="product-info__content">
										<div class="item-name">
											<h3> <a href="#">Bánh xèo thịt băm</a></h3>
										</div>
										<div class="item-price">
											<h4> <span>90,000</span><span>đ</span></h4>
										</div>
										<div class="item-number">
											<p>số lượng:<span>1</span></p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="cart-layer cart-modal">
								<div class="cart-layer__title">
									<h2>giỏ hàng của bạn hiện có <span>2 </span>sản phẩm</h2>
								</div>
								<div class="cart-layer__sum">
									<p>Giá đơn hàng: <span>1,230,000 đ</span></p>
								</div>
								<div class="cart-layer__act">
									<ul class="list-inline">
										<li><a href="#">
												<button>Tiếp tục mua hàng<i class="fa fa-chevron-right"></i></button></a></li>
										<li><a href="#">
												<button>Đặt hàng và thanh toán<i class="fa fa-chevron-right"></i></button></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--.add-cart-popup-wrap-->
	</div>
<?php get_footer( ) ?>