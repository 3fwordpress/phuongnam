<?php 
/**
 * Page file, the file show a single page
 * @author 3F Wordpress Team 
 * @link http://3fgroup.vn
 */
?>


<?php get_header(); ?>
<?php while(have_posts()){ 
	the_post(); ?>
<div class="about-page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="title-default"> 
					<h1><?php the_title( $before = '', $after = '', $echo = true ) ?></h1>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="content-wrapper">
					<?php the_content( ) ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php get_footer(); ?>