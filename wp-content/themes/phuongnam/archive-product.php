<?php 
/*
* author: Nguyen Trong Giap
*/
?>
<?php get_header( ) ?>
<section class="set-menu" id="set-menu">
	<div class="topbar topbar-wrap">
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<ul class="list-inline">
							<?php //path_from_province_archive(); ?>
							<li> <a href="#">Trang chủ</a></li>
							<li class="li-display"><a href="#">Danh mục</a></li>
							<li><a href="#">Set Menu tại nhà hàng Phương Nam</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--.topbar-wrap-->
	<div class="goodsale-content">
		<div class="container">
			<div class="row">
				<div class="main-content col-sm-12">
					<div class="row">
						<div class="col-lg-3 col-sm-12 col-3">
							<aside class="sidebar listmenu1">
								<div class="listmenu listmenu-wrap">
									<div class="listmenu__title">
										<h4>Thực đơn chọn món</h4>
									</div>
									<ul>
									</ul>
								</div>
								<!--.listmenu-wrap-->
								<div class="lst-item lst-item-wrap">
									<div class="listmenu__title">
										<h4>Món ngon nổi bật</h4>
									</div>
									<div class="owl-carousel">
										<div class="lst-item__content">
											<div class="item-sidebar2 item-sidebar2-wrap">
												<div class="seller-item">
													<div class="seller-img"><a href="#"><img src="images/anh1.jpg" alt=""/></a></div>
													<div class="product-content">
														<h5><a href="#">Bánh Tráng Trảng Bàng Cuốn Chân Giò Luộc</a></h5>
														<h3 class="price"><span>155,000</span><span><sup>
																	<u>đ</u></sup></span></h3>
													</div>
													<div class="clear-fix"></div>
												</div>
											</div>
											<!--.item-sidebar2-wrap-->
											<div class="item-sidebar2 item-sidebar2-wrap">
												<div class="seller-item">
													<div class="seller-img"><a href="#"><img src="images/anh1.jpg" alt=""/></a></div>
													<div class="product-content">
														<h5><a href="#">Bánh Tráng Trảng Bàng Cuốn Chân Giò Luộc</a></h5>
														<h3 class="price"><span>155,000</span><span><sup>
																	<u>đ</u></sup></span></h3>
													</div>
													<div class="clear-fix"></div>
												</div>
											</div>
											<!--.item-sidebar2-wrap-->
										</div>
										<div class="lst-item__content">
											<div class="item-sidebar2 item-sidebar2-wrap">
												<div class="seller-item">
													<div class="seller-img"><a href="#"><img src="images/anh1.jpg" alt=""/></a></div>
													<div class="product-content">
														<h5><a href="#">Bánh Tráng Trảng Bàng Cuốn Chân Giò Luộc</a></h5>
														<h3 class="price"><span>155,000</span><span><sup>
																	<u>đ</u></sup></span></h3>
													</div>
													<div class="clear-fix"></div>
												</div>
											</div>
											<!--.item-sidebar2-wrap-->
											<div class="item-sidebar2 item-sidebar2-wrap">
												<div class="seller-item">
													<div class="seller-img"><a href="#"><img src="images/anh1.jpg" alt=""/></a></div>
													<div class="product-content">
														<h5><a href="#">Bánh Tráng Trảng Bàng Cuốn Chân Giò Luộc</a></h5>
														<h3 class="price"><span>155,000</span><span><sup>
																	<u>đ</u></sup></span></h3>
													</div>
													<div class="clear-fix"></div>
												</div>
											</div>
											<!--.item-sidebar2-wrap-->
										</div>
										<div class="lst-item__content">
											<div class="item-sidebar2 item-sidebar2-wrap">
												<div class="seller-item">
													<div class="seller-img"><a href="#"><img src="images/anh1.jpg" alt=""/></a></div>
													<div class="product-content">
														<h5><a href="#">Bánh Tráng Trảng Bàng Cuốn Chân Giò Luộc</a></h5>
														<h3 class="price"><span>155,000</span><span><sup>
																	<u>đ</u></sup></span></h3>
													</div>
													<div class="clear-fix"></div>
												</div>
											</div>
											<!--.item-sidebar2-wrap-->
											<div class="item-sidebar2 item-sidebar2-wrap">
												<div class="seller-item">
													<div class="seller-img"><a href="#"><img src="images/anh1.jpg" alt=""/></a></div>
													<div class="product-content">
														<h5><a href="#">Bánh Tráng Trảng Bàng Cuốn Chân Giò Luộc</a></h5>
														<h3 class="price"><span>155,000</span><span><sup>
																	<u>đ</u></sup></span></h3>
													</div>
													<div class="clear-fix"></div>
												</div>
											</div>
											<!--.item-sidebar2-wrap-->
										</div>
									</div>
								</div>
							</aside>
						</div>
						<div class="col-lg-9 col-sm-12 col-9">
							<div class="row">
								<div class="col9-content">
									<div class="title">
										<h4>Set Menu</h4>
									</div>
									<div class="toolbar">
										<div class="row">
											<div class="col-lg-12 col-sm-12">
												<div class="toolbar-box fleft">
													<ul class="list-inline fleft">
														<li><a class="large" href="#"><i class="fa fa-th-large" aria-hidden="true"></i></a></li>
														<li><a class="list" href="#"><i class="fa fa-list-ul" aria-hidden="true"></i></a></li>
													</ul>
													<div class="sort-tag fleft"><span>Sắp xếp theo:</span>
														<select name="sort">
															<option value="sp">Sản phẩm nổi bật</option>
															<option value="up">Giá: Tăng dần</option>
															<option value="down">Giá: Giảm dần</option>
															<option value="az">Tên: A - Z</option>
															<option value="za">Tên: Z - A</option>
														</select>
													</div>
													<div class="clear-fix"></div>
												</div>
												<div class="clear-fix"></div>
											</div>
										</div>
									</div>
									<div class="menu-content">
										<div class="row">
											<div class="col-sm-4 col-xs-6">
												<div class="product product-wrap component">
													<div class="product-info"><a href="#">
															<div class="product-info__img"><img src="images/ta_product.jpg" alt=""/></div>
															<div class="product-info__act">
																<ul class="list-inline">
																	<li class="add-cart"><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																	<li class="info"><a class="img" href="#"></a></li>
																	<li class="view"><span class="img" data-toggle="modal" data-target="#view-product-popup"></span></li>
																</ul>
															</div></a></div>
													<div class="product-box">
														<div class="product-box__name fleft"><a href="#">Bánh xèo thịt băm</a></div>
														<div class="product-box__price fright">
															<h4>90,000<sup> 
																	<u>đ</u></sup></h4>
														</div>
														<div class="clear-fix"></div>
													</div>
												</div>
												<!--.product-wrap-->
											</div>
											<div class="col-sm-4 col-xs-6">
												<div class="product product-wrap component">
													<div class="product-info"><a href="#">
															<div class="product-info__img"><img src="images/ta_images-zoom4.jpg" alt=""/></div>
															<div class="product-info__act">
																<ul class="list-inline">
																	<li class="add-cart"><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																	<li class="info"><a class="img" href="#"></a></li>
																	<li class="view"><span class="img" data-toggle="modal" data-target="#view-product-popup"></span></li>
																</ul>
															</div></a></div>
													<div class="product-box">
														<div class="product-box__name fleft"><a href="#">Bánh xèo thịt băm</a></div>
														<div class="product-box__price fright">
															<h4>90,000<sup> 
																	<u>đ</u></sup></h4>
														</div>
														<div class="clear-fix"></div>
													</div>
												</div>
												<!--.product-wrap-->
											</div>
											<div class="col-sm-4 col-xs-6">
												<div class="product product-wrap component">
													<div class="product-info"><a href="#">
															<div class="product-info__img"><img src="images/ta_images-zoom5.jpg" alt=""/></div>
															<div class="product-info__act">
																<ul class="list-inline">
																	<li class="add-cart"><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																	<li class="info"><a class="img" href="#"></a></li>
																	<li class="view"><span class="img" data-toggle="modal" data-target="#view-product-popup"></span></li>
																</ul>
															</div></a></div>
													<div class="product-box">
														<div class="product-box__name fleft"><a href="#">Bánh xèo thịt băm</a></div>
														<div class="product-box__price fright">
															<h4>90,000<sup> 
																	<u>đ</u></sup></h4>
														</div>
														<div class="clear-fix"></div>
													</div>
												</div>
												<!--.product-wrap-->
											</div>
											<div class="col-sm-4 col-xs-6">
												<div class="product product-wrap component">
													<div class="product-info"><a href="#">
															<div class="product-info__img"><img src="images/ta_images-zoom2.jpg" alt=""/></div>
															<div class="product-info__act">
																<ul class="list-inline">
																	<li class="add-cart"><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																	<li class="info"><a class="img" href="#"></a></li>
																	<li class="view"><span class="img" data-toggle="modal" data-target="#view-product-popup"></span></li>
																</ul>
															</div></a></div>
													<div class="product-box">
														<div class="product-box__name fleft"><a href="#">Bánh xèo thịt băm</a></div>
														<div class="product-box__price fright">
															<h4>90,000<sup> 
																	<u>đ</u></sup></h4>
														</div>
														<div class="clear-fix"></div>
													</div>
												</div>
												<!--.product-wrap-->
											</div>
											<div class="col-sm-4 col-xs-6">
												<div class="product product-wrap component">
													<div class="product-info"><a href="#">
															<div class="product-info__img"><img src="images/ta_images-zoom3.jpg" alt=""/></div>
															<div class="product-info__act">
																<ul class="list-inline">
																	<li class="add-cart"><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																	<li class="info"><a class="img" href="#"></a></li>
																	<li class="view"><span class="img" data-toggle="modal" data-target="#view-product-popup"></span></li>
																</ul>
															</div></a></div>
													<div class="product-box">
														<div class="product-box__name fleft"><a href="#">Bánh xèo thịt băm</a></div>
														<div class="product-box__price fright">
															<h4>90,000<sup> 
																	<u>đ</u></sup></h4>
														</div>
														<div class="clear-fix"></div>
													</div>
												</div>
												<!--.product-wrap-->
											</div>
											<div class="col-sm-4 col-xs-6">
												<div class="product product-wrap component">
													<div class="product-info"><a href="#">
															<div class="product-info__img"><img src="images/ta_product.jpg" alt=""/></div>
															<div class="product-info__act">
																<ul class="list-inline">
																	<li class="add-cart"><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																	<li class="info"><a class="img" href="#"></a></li>
																	<li class="view"><span class="img" data-toggle="modal" data-target="#view-product-popup"></span></li>
																</ul>
															</div></a></div>
													<div class="product-box">
														<div class="product-box__name fleft"><a href="#">Bánh xèo thịt băm</a></div>
														<div class="product-box__price fright">
															<h4>90,000<sup> 
																	<u>đ</u></sup></h4>
														</div>
														<div class="clear-fix"></div>
													</div>
												</div>
												<!--.product-wrap-->
											</div>
										</div>
									</div>
									<div class="menu-content2">
										<div class="row">
											<div class="col-sm-12">
												<div class="list-product list-product-wrap">
													<div class="list-wrap">
														<div class="row">
															<div class="col-sm-5"><a class="fleft" href="#">
																	<div class="product-info__img"><img src="images/ta_product.jpg" alt=""/>
																		<div class="layer-prd"><i class="fa fa-search" aria-hidden="true"></i></div>
																	</div></a></div>
															<div class="col-sm-7">
																<div class="list-prd__infor fleft"><a href="#">
																		<h3>Bánh xèo thịt băm</h3></a>
																	<h4>90,000</h4>
																	<p>Là Set Menu dành cho 5 - 6 người. Set Họp Mặt 2 bao gồm 7 món: 1. Bánh tráng cuốn Ba chỉ quay/Bò tơ củ chi;  2. Bò xiên tăm; 3. Cá kèo nướng muối ớt; 4. Cơm...</p>
																	<ul class="list-inline">
																		<li><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																		<li><a class="img" href="#"></a></li>
																		<li><span class="img" data-toggle="modal" data-target="#view-product-popup"></span></li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--.list-product-wrap-->
												<div class="list-product list-product-wrap">
													<div class="list-wrap">
														<div class="row">
															<div class="col-sm-5"><a class="fleft" href="#">
																	<div class="product-info__img"><img src="images/ta_images-zoom4.jpg" alt=""/>
																		<div class="layer-prd"><i class="fa fa-search" aria-hidden="true"></i></div>
																	</div></a></div>
															<div class="col-sm-7">
																<div class="list-prd__infor fleft"><a href="#">
																		<h3>Bánh xèo thịt băm</h3></a>
																	<h4>90,000</h4>
																	<p>Là Set Menu dành cho 5 - 6 người. Set Họp Mặt 2 bao gồm 7 món: 1. Bánh tráng cuốn Ba chỉ quay/Bò tơ củ chi;  2. Bò xiên tăm; 3. Cá kèo nướng muối ớt; 4. Cơm...</p>
																	<ul class="list-inline">
																		<li><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																		<li><a class="img" href="#"></a></li>
																		<li><span class="img" data-toggle="modal" data-target="#view-product-popup"></span></li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--.list-product-wrap-->
												<div class="list-product list-product-wrap">
													<div class="list-wrap">
														<div class="row">
															<div class="col-sm-5"><a class="fleft" href="#">
																	<div class="product-info__img"><img src="images/ta_images-zoom5.jpg" alt=""/>
																		<div class="layer-prd"><i class="fa fa-search" aria-hidden="true"></i></div>
																	</div></a></div>
															<div class="col-sm-7">
																<div class="list-prd__infor fleft"><a href="#">
																		<h3>Bánh xèo thịt băm</h3></a>
																	<h4>90,000</h4>
																	<p>Là Set Menu dành cho 5 - 6 người. Set Họp Mặt 2 bao gồm 7 món: 1. Bánh tráng cuốn Ba chỉ quay/Bò tơ củ chi;  2. Bò xiên tăm; 3. Cá kèo nướng muối ớt; 4. Cơm...</p>
																	<ul class="list-inline">
																		<li><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																		<li><a class="img" href="#"></a></li>
																		<li><span class="img" data-toggle="modal" data-target="#view-product-popup"></span></li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--.list-product-wrap-->
												<div class="list-product list-product-wrap">
													<div class="list-wrap">
														<div class="row">
															<div class="col-sm-5"><a class="fleft" href="#">
																	<div class="product-info__img"><img src="images/ta_images-zoom2.jpg" alt=""/>
																		<div class="layer-prd"><i class="fa fa-search" aria-hidden="true"></i></div>
																	</div></a></div>
															<div class="col-sm-7">
																<div class="list-prd__infor fleft"><a href="#">
																		<h3>Bánh xèo thịt băm</h3></a>
																	<h4>90,000</h4>
																	<p>Là Set Menu dành cho 5 - 6 người. Set Họp Mặt 2 bao gồm 7 món: 1. Bánh tráng cuốn Ba chỉ quay/Bò tơ củ chi;  2. Bò xiên tăm; 3. Cá kèo nướng muối ớt; 4. Cơm...</p>
																	<ul class="list-inline">
																		<li><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																		<li><a class="img" href="#"></a></li>
																		<li><span class="img" data-toggle="modal" data-target="#view-product-popup"></span></li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--.list-product-wrap-->
												<div class="list-product list-product-wrap">
													<div class="list-wrap">
														<div class="row">
															<div class="col-sm-5"><a class="fleft" href="#">
																	<div class="product-info__img"><img src="images/ta_images-zoom3.jpg" alt=""/>
																		<div class="layer-prd"><i class="fa fa-search" aria-hidden="true"></i></div>
																	</div></a></div>
															<div class="col-sm-7">
																<div class="list-prd__infor fleft"><a href="#">
																		<h3>Bánh xèo thịt băm</h3></a>
																	<h4>90,000</h4>
																	<p>Là Set Menu dành cho 5 - 6 người. Set Họp Mặt 2 bao gồm 7 món: 1. Bánh tráng cuốn Ba chỉ quay/Bò tơ củ chi;  2. Bò xiên tăm; 3. Cá kèo nướng muối ớt; 4. Cơm...</p>
																	<ul class="list-inline">
																		<li><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																		<li><a class="img" href="#"></a></li>
																		<li><span class="img" data-toggle="modal" data-target="#view-product-popup"></span></li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--.list-product-wrap-->
												<div class="list-product list-product-wrap">
													<div class="list-wrap">
														<div class="row">
															<div class="col-sm-5"><a class="fleft" href="#">
																	<div class="product-info__img"><img src="images/ta_product.jpg" alt=""/>
																		<div class="layer-prd"><i class="fa fa-search" aria-hidden="true"></i></div>
																	</div></a></div>
															<div class="col-sm-7">
																<div class="list-prd__infor fleft"><a href="#">
																		<h3>Bánh xèo thịt băm</h3></a>
																	<h4>90,000</h4>
																	<p>Là Set Menu dành cho 5 - 6 người. Set Họp Mặt 2 bao gồm 7 món: 1. Bánh tráng cuốn Ba chỉ quay/Bò tơ củ chi;  2. Bò xiên tăm; 3. Cá kèo nướng muối ớt; 4. Cơm...</p>
																	<ul class="list-inline">
																		<li><a class="button" data-toggle="modal" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a></li>
																		<li><a class="img" href="#"></a></li>
																		<li><span class="img" data-toggle="modal" data-target="#view-product-popup"></span></li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--.list-product-wrap-->
											</div>
										</div>
									</div>
									<div class="Pagination">
										<div class="row">
											<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a class="prev" href="#"><span><i class="fa fa-angle-left"></i>&nbsp;Trang trước</span></a></div>
											<div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
												<ul class="list-inline">
													<li><a class="a-active" href="#">1</a></li>
													<li><a href="#">2</a></li>
													<li><a href="#">3</a></li>
													<li><a href="#">4</a></li>
													<li class="dots"><a href="#">...</a></li>
													<li><a href="#">10</a></li>
												</ul>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 text-right"><a class="next" href="#"><span>Trang sau&nbsp;<i class="fa fa-angle-right"></i></span></a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="up up-active"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
</section>
<?php get_footer( ) ?>