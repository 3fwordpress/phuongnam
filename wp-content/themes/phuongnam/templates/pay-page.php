<?php 

/*
*Template Name: Pay page
*/
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, shrink-to-fit=no, user-scalable=no"/>
		<meta name="keywords" content="coding, html, css"/>
		<!-- Styles-->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"/>
		<link rel="stylesheet" href="<?php echo TFT_URL; ?>/public/libs/bootstrap-3/css/bootstrap-theme.min.css"/>
		<link rel="stylesheet" href="<?php echo TFT_URL; ?>/public/libs/bootstrap-3/css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo TFT_URL; ?>/public/libs/owlCarousel/assets/owl.theme.default.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo TFT_URL; ?>/public/libs/owlCarousel/assets/owl.carousel.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo TFT_URL; ?>/public/libs/owlCarousel/assets/animate.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo TFT_URL; ?>/public/libs/flexslider/flexslider.min.css"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"/>
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"/>
		<link rel="stylesheet" type="text/css" href="<?php echo TFT_URL; ?>/public/style.css"/>
		<style type="text/css">
			.form-row{
				width: 100%;
			}
		</style>
		<?php wp_head() ?>
	</head>
	<body <?php body_class() ?>>
		<?php echo do_shortcode('[woocommerce_checkout]') ?>
		<!--#wrapper-->
		<!--JS-->
		<script src="<?php echo TFT_URL; ?>/public/libs/jQuery/jquery.min.js"></script>
		<script src="<?php echo TFT_URL; ?>/public/libs/owlCarousel/owl.carousel.min.js"></script>
		<script src="<?php echo TFT_URL; ?>/public/libs/validate/jquery.validate.min.js"></script>
		<script src="<?php echo TFT_URL; ?>/public/libs/validate/additional-methods.min.js"></script>
		<script src="<?php echo TFT_URL; ?>/public/libs/elevatezoom/jquery.elevatezoom.js"></script>
		<script src="<?php echo TFT_URL; ?>/public/libs/flexslider/jquery.flexslider-min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="<?php echo TFT_URL; ?>/public/js/script.js"></script>
		<script type="text/javascript">
			$(function() {
				$('#ahihi_click').click(function(event) {
					event.preventDefault();
					$('.checkout_coupon #coupon_code').val($('#ahihi_input').val());
					console.log($('.checkout_coupon #coupon_code').val());
					
					$('.checkout_coupon .form-row-last button').click();
				});
			});
		</script>
		<script type="text/javascript" >
			$(window).on('load',function(event) {
				// event.preventDefault();
				// $('.shiping-price .fright').html('<ul style="list-style: none;" class="ul_child">'+$('.shipping td #shipping_method').html()+'</ul>');
				// $('.ul_child li input').click(function(event) {
				// 	console.log($(this).parent("li").index());
				// 	var id = $(this).parent("li").index();
				// });
			});
		</script>
		<?php wp_footer() ?>
	</body>
</html>