<?php 
/*
*Template Name: Log-in page
*/
?>
<?php get_header(  ) ?>
<div class="login-page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="title-default"> 
					<h1>Đăng nhập</h1>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="content-wrapper text-center">
					<form id="dang_nhap" method="POST" action="">
						<div class="email_input">
							<label class="icon-field email-icon fleft"><i class="icon-login icon-email"></i></label>
							<input class="login_input fleft" id="customer_email" type="email" placeholder="Email" name="email"/>
							<div class="clear-fix"></div>
						</div>
						<div class="pass_input">
							<label class="icon-field pass-icon fleft"><i class="icon-login icon-pass"></i></label>
							<input class="fleft login_input" id="customer_pass" type="password" placeholder="Mật khẩu" name="password"/>
							<div class="clear-fix"></div>
						</div>
						<div class="action_bottom">
							<button type="submit" name="submit_singin">Đăng nhập</button>
						</div>
						<div class="req_pass">
							<p><span> <a href="#">Quên mật khẩu? </a></span>hoặc<span> <a href="#">Đăng ký</a></span></p>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



<?php get_footer(  ) ?>