<?php 
/*
*Template Name: Register page
*/
?>
<?php get_header(  ) ?>
<div class="singin-page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="title-default"> 
					<h1>Tạo tài khoản</h1>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="content-wrapper text-center">
					<form id="dang_ki" method="POST" action="" >
						<div class="first-name_input">
							<label class="icon-field user-icon fleft"><i class="icon-login icon-user"></i></label>
							<input class="login_input fleft" id="customer_name" type="text" placeholder="Họ" name="firstname"/>
							<div class="clear-fix"></div>
						</div>
						<div class="name_input">
							<label class="icon-field user-icon fleft"><i class="icon-login icon-user"></i></label>
							<input class="login_input fleft" id="customer_first-name" type="text" placeholder="Tên" name="lastname"/>
							<div class="clear-fix"></div>
						</div>
						<div class="email_input">
							<label class="icon-field email-icon fleft"><i class="icon-login icon-email"></i></label>
							<input class="login_input fleft" id="customer_email" type="email" placeholder="Email" name="email"/>
							<div class="clear-fix"></div>
						</div>
						<div class="pass_input">
							<label class="icon-field pass-icon fleft"><i class="icon-login icon-pass"></i></label>
							<input class="fleft login_input" id="customer_pass" type="password" placeholder="Mật khẩu" name="password"/>
							<div class="clear-fix"></div>
						</div>
						<div class="action_bottom">
							<button type="submit" name="submit_register">Đăng ký</button>
						</div>
						<div class="req_pass">
							<p><span> <a href="<?php echo home_url( ) ?>">Quay về</a></span></p>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>