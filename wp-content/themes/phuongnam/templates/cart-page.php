<?php
/* 
    Template Name: Cart Page
*/
?>
<?php get_header( ) ?>
	<div class="cart-page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="title-default"> 
						<h1>Giỏ hàng</h1>
					</div>
				</div>
				<div class="col-xs-12">
					<form id="cart-form" action="">
						<table> 
							<thead>
								<tr>
									<th class="image">&nbsp;</th>
									<th class="item">Tên sản phẩm</th>
									<th class="qty">Số lượng</th>
									<th class="price">Giá tiền</th>
									<th class="remove">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
							<?php
							global $woocommerce;
							$items = $woocommerce->cart->get_cart();
							$tong = 0;
							foreach($items as $item => $values) { 
								$_product =  wc_get_product( $values['data']->get_id()); 
								$price = get_post_meta($values['product_id'] , '_price', true);
								$tong += ($price*$values['quantity']);
								$img_avt = get_the_post_thumbnail_url( $values['product_id'] );
								$img_avt = !$img_avt?(TFT_URL.'/no_photo.jpg'):$img_avt;
								?>
								<tr class="active">
									<td class="image"><img src="<?php echo $img_avt ?>" alt=""/></td>
									<td class="item"> <strong><a href="<?php echo get_permalink( $_product->get_ID() ) ?>"><?php echo $_product->get_title() ?></a></strong></td>
									<td class="qty"> 
										<input type="number" value="<?php echo $values['quantity'] ?>" min="1"/>
									</td>
									<td class="price"> <span><?php echo wc_price($price*$values['quantity']) ?></span></td>
									<td class="remove"> <a class="remove" id="<?php echo $values['key'] ?>" href="#">xóa</a></td>
								</tr>
								<?php
							}
							?>
							</tbody>
							<tfoot style="font-family: 'arial';">
								<tr class="show-table">
									<td class="image"></td>
									<td class="item"> <a href="#"></a></td>
									<td class="qty"> <strong>Tổng cộng</strong></td>
									<td class="price"> <span><?php echo wc_price($tong) ?></span></td>
									<td class="remove"></td>
								</tr>
							</tfoot>
						</table>
						<script type="text/javascript">
							jQuery(document).ready(function($) {
								$('tbody tr td a.remove').click(function(event) {
									event.preventDefault();
									var product_id = $(this).attr('id');
									$.ajax({
									    type: 'POST',
									    data: {
									        action: 'remove-cart-item',
									        cartid : product_id,
									        nonce: WPAjax.nonce,
									    },
									    url: WPAjax.url,
									    beforeSend: function() {

									    },
									    success: function(data) {
									    	load_cart();
									        alert("Xóa khỏi giỏ hàng thành công");
									    }
									});
								});
								// $('#add-cart').click(function(event) {
								// 	event.preventDefault();
								// 	var number_item = $('.quantity-val').val();
								// 	
								// })
							});
						</script>
						<div class="row">
							<div class="col-md-6">
								<div class="note-form">
									<label>Ghi chú</label><br/>
									<textarea id="note" name="note" rows="8" cols="50"></textarea>
								</div>
							</div>
							<div class="col-md-6 cart-buttons">
								<div class="button-checkout">
									<a class="checkout fright checkout-link" href="<?php echo home_url('thanh-toan'); ?>" name="checkout">Thanh toán</a>
									<a class="update fright" type="" id="update_cart" name="checkout">Cập nhật</a>
									<clear-fix></clear-fix>
								</div>
							</div>
							<script type="text/javascript">
								$(document).ready(function() {
									$('.checkout-link').click(function(event) {
										console.log("ss");
										// window.location = "<?php echo home_url('thanh-toan'); ?>";
									});
									$('#update_cart').click(function(event) {
										event.preventDefault();
										var cart =new Array();
										var count =0;
										$('tbody tr').each(function(index, el) {
											element = {};
											element.quantity = $($(this)[0].cells[2]).children().val();
											element.key = $($(this)[0].cells[4]).children().attr('id');
											cart.push({ element });
											count++;
										});

										$.ajax({
									    type: 'POST',
									    data: {
									        action: 'update-cart-items',
									        obj_update : (cart),
									        nonce: WPAjax.nonce,
									    },
									    url: WPAjax.url,
									    beforeSend: function() {

									    },
									    success: function(data) {
									    	load_cart();
									        $('#cart-form table').html(data);
									        alert("Cập nhật giỏ hàng thành công");
									        // window.location = window.location;
									    }
									});
									});
								});
							</script>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(  ) ?>