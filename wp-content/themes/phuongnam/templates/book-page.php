<?php 
/*
*Template Name: Book Page
*/
?>
<?php get_header() ?>
<section class="set-menu" id="set-menu">
	<div class="topbar topbar-wrap">
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<ul class="list-inline">
							<li> <a href="#">Trang chủ</a></li>
							<li class="li-display"><a href="#">Danh mục</a></li>
							<li><a href="#">Họp Mặt tại nhà hàng phương Nam</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--.topbar-wrap-->
	<div class="goodsale-content">
		<div class="container">
			<div class="row">
				<div class="main-content col-sm-12">
					<div class="row">
						<div class="col-md-3 col-sm-12">
							<aside class="sidebar listmenu1">
								<?php do_action( 'archive_product_left_hook' ) ?>
							</aside>
						</div>
						<div class="col-md-9 col-sm-12 col-9">
							<div class="page-title">
								<div class="page-title__left fleft">
									<h4>Set menu</h4>
								</div>
								<div class="page-title__right fright">
									<?php 
									$_terms = get_terms( array(
										'taxonomy' => 'thuc-don',
										'hide_empty' => false,
									) );
									$count =1;
									?>
									<ul class="list-inline list-inline-menu">
										<?php foreach ($_terms as $key => $value): ?>
											<li <?= $count==1?'class="active"':'' ?>><a the-id="<?php echo $value->term_id ?>" href="#"><?php echo $value->name ?></a></li>
											<?php $count++; ?>
										<?php endforeach ?>
									</ul>
								</div>
								<div class="clear-fix"></div>
							</div>
							<div class="page-product">
								<div class="row">
									<?php
									$args = array(
										'posts_per_page' => -1,
										'post_type' => 'product',
										'tax_query' => array(
											array(
												'taxonomy' => 'thuc-don',
												'field' => 'term_id',
												'terms' => $_terms[0]->term_id,
											)
										)
									);
									$_posts = get_posts( $args ); 
									// echo '<pre>'.__FILE__.'::'.__METHOD__.'('.__LINE__.')<br>'; 
									// 	print_r($_posts); 
									// echo '</pre>';
									?>
									<?php foreach ($_posts as $key => $value): ?>
										<?php $product = wc_get_product( $value->ID ); ?>
										<div class="col-sm-4 col-xs-6">
											<div class="product product-wrap component">
												<div class="product-info"><a href="#">
														<div class="product-info__img"><img src="<?php echo get_the_post_thumbnail_url( $value->ID ) ?>" alt=""/></div>
														<div class="product-info__act">
															<ul class="list-inline">
																<li class="add-cart">
																	<a class="button add_to_cart" data-toggle="modal" the-id="<?php echo $value->ID ?>" data-target="#add-cart-popup"><span>Thêm vào giỏ</span></a>
																</li>
																<li class="info">
																	<a class="img" href="<?php echo get_permalink( $value->ID  ) ?>"></a>
																</li>
																<li class="view">
																	<span class="img view-popupx" data-toggle="modal" data-target="#view-product-popup" the-id="<?php echo $value->ID ?>"></span>
																</li>
															</ul>
														</div></a></div>
												<div class="product-box">
													<div class="product-box__name fleft"><a href="#"><?php echo $product->get_title() ?></a></div>
													<div class="product-box__price fright">
														<h4><?php echo wc_price($product->get_price()); ?></h4>
													</div>
													<div class="clear-fix"></div>
												</div>
											</div>
											<!--.product-wrap-->
										</div>
									<?php endforeach ?>
								</div>
							</div>
							<script type="text/javascript">
								$(function() {
									$('.list-inline-menu li a').click(function(event) {
										// console.log($(this));
										//console.log($(this).attr('the-id'));
										var id_product= $(this).attr('the-id');;
										console.log(id_product);
										$.ajax({
											type: 'POST',
											data: {
												action: 'load-produts-of-memu',
												productID: id_product,
												nonce: WPAjax.nonce,
											},
											url: WPAjax.url,
											beforeSend: function() {
												var xxx = '<i style="font-size: 100px;color:black;width: 100%;text-align: center;" class="fa fa-refresh fa-spin fa-3x fa-fw" aria-hidden="true"></i>';
												$('.page-product .row').html(xxx);
											},
											success: function(data) {
												$('.page-product .row').html(data);
												load_cart();
											}
										});
									});
								});
							</script>
							<div class="page-title">
								<div class="page-title__left fleft">
									<h4>Tùy chọn riêng</h4>
								</div>
								<div class="clear-fix"></div>
							</div>
							<div class="menu__tab-product">
								<ul class="nav nav-pills text-center list-inline">
									<li class="active"><a href="#">Cuống</a></li>
									<li><a href="#">Lẩu</a></li>
									<li><a href="#">Nướng</a></li>
									<li><a href="#">Món ăn nhẹ</a></li>
									<li><a href="#">Bánh tráng trảng bàng</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<div class="order-title text-center">
						<h3>Đặt bàn</h3>
					</div>
					<div class="order-content">
						<form id="order-form" action="" method="post">
							<div class="row info-order">
								<div class="col-sm-4">
									<input type="text" value="Nguyễn Trọng Giáp" placeholder="Họ tên" name="name"/>
								</div>
								<div class="col-sm-4">
									<input type="text" value="0123456789" placeholder="Số điện thoại" name="number"/>
								</div>
								<div class="col-sm-4">
									<input type="text" value="12" placeholder="Số người" name="number-human"/>
								</div>
								<div class="col-sm-4">
								<?php $_bases = get_field('pn_add_base','option'); ?>
									<select id="choose-place" name="base-choose">
										<option disabled selected hidden>chọn cơ sở</option>
										<?php 
										$count_base =1;
										foreach ($_bases as $key => $value): ?>
											<option value="bases-<?php echo $count_base ?>">Cơ sở <?php echo $count_base ?>: <?php echo $value['pn_location_base'] ?></option>
											<?php $count_base++; ?>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4">
									<input type="date" placeholder="Ngày đăt bàn" name="date-order"/>
								</div>
								<div class="col-sm-4">
									<input type="time" placeholder="Giờ đặt bàn" name="time-order"/>
								</div>
							</div>
							<?php 
							unset($_terms);
							$_terms = get_terms( array(
								'taxonomy' => 'product_cat',
								'hide_empty' => false,
							) );
							// echo '<pre>'.__FILE__.'::'.__METHOD__.'('.__LINE__.')<br>'; 
							// 	print_r($_terms); 
							// echo '</pre>';
							$_max_terms = count($_terms);
							?>
							<div class="menu-list">

								<?php $count=0; ?>
								<?php foreach ($_terms as $key => $value){ 
									// echo '<pre>'.__FILE__.'::'.__METHOD__.'('.__LINE__.')<br>'; 
									// 	print_r($value); 
									// echo '</pre>';
									?>
									<?php if ($count%2==0){
										if ($count==$_max_terms-1) { 
										?>
										<div class="row menu-list">
											<div class="col-sm-5">
												<h3 class="title"><?php echo $value->name ?></h3>
												<?php for ($i=0; $i <5 ; $i++) { ?>
													<?php
													$args = array(
														'post_type' => 'product',
														'tax_query' => array(
															array(
																'taxonomy' => 'product_cat',
																'field' => 'term_id',
																'terms' => $value->term_id
															)
														)
													);
													$_posts = get_posts( $args ); 
													?>
													<select name="<?php echo $value->slug ?>[]">
														<option disabled selected hidden>Chọn món</option>
														<?php 
														foreach ($_posts as  $value_2): ?>
														<option value="<?php echo $value_2->ID ?>"><?php echo $value_2->post_title ?></option>
														<?php endforeach ?>
													</select>
												<?php } ?> 
											</div>
										</div>
										<?php break;
										}else{
										?>
										<div class="row menu-list">
											<div class="col-sm-5">
												<h3 class="title"><?php echo $value->name ?></h3>
												<?php for ($i=0; $i <5 ; $i++) { ?>
													<?php
													$args = array(
														'post_type' => 'product',
														'tax_query' => array(
															array(
																'taxonomy' => 'product_cat',
																'field' => 'term_id',
																'terms' => $value->term_id
															)
														)
													);
													$_posts = get_posts( $args ); 
													?>
													<select name="<?php echo $value->slug ?>[]">
														<option disabled selected hidden>Chọn món</option>
														<?php 
														foreach ($_posts as  $value_2): ?>
														<option value="<?php echo $value_2->ID ?>"><?php echo $value_2->post_title ?></option>
														<?php endforeach ?>
													</select>
												<?php } ?> 
											</div>
										<?php
										}
									} else {
										?>
											<div class="col-sm-5 col-sm-offset-2">
												<h3 class="title"><?php echo $value->name ?></h3>
												<?php for ($i=0; $i <5 ; $i++) { ?>
													<?php
													$args = array(
														'post_type' => 'product',
														'tax_query' => array(
															array(
																'taxonomy' => 'product_cat',
																'field' => 'term_id',
																'terms' => $value->term_id
															)
														)
													);
													$_posts = get_posts( $args ); 
													?>
													<select name="<?php echo $value->slug ?>[]">
														<option disabled selected hidden>Chọn món</option>
														<?php 
														foreach ($_posts as  $value_2): ?>
														<option value="<?php echo $value_2->ID ?>"><?php echo $value_2->post_title ?></option>
														<?php endforeach ?>
													</select>
												<?php } ?> 
											</div>
										</div>
										<?php
									}
									$count++; ?>
								<?php } ?>

								<?php for ($i=0; $i <5 ; $i++) { ?>
										<!-- select name="lau">
											<option value="choose-place" selected="selected">Chọn món</option>
											<option value="ha-noi">Lẩu</option>
											<option value="ha-noi">Lẩu</option>
										</select> -->
								<?php } ?> 
								<div class="messenger">
									<textarea name="message_book" cols="30" rows="1" placeholder="lời nhắn"></textarea>
								</div>
								<div class="form-submit">
									<div class="fleft ul list-inline">
										<li>
											<button class="active" type="button">Đặt bàn</button>
										</li>
										<li>
											<button type="button">Tư vấn cho tôi</button>
										</li>
									</div>
									<div class="fright ul list-inline">
										<li>
											<button type="submit" name="submit_book">Gửi đi</button>
										</li>
									</div>
									<div class="clear-fix"></div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php get_footer() ?>