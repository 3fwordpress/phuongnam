<?php 
/**
 * Search file, the file show all result for a key word
 * @author 3F Wordpress Team
 * @link http://3fgroup.vn
 */
?>

<?php get_header(); ?>
      
<?php if (have_posts()):

    while (have_posts()) : the_post();?> 
        <img src="<?php echo the_post_thumbnail_url('tw_thumbnail') ?>" alt="<?php the_title() ?>" />
        <h2><?php the_title() ?></h2>

        <p><?php the_excerpt(); ?></p>
    <?php endwhile;?> 

    <div class="nav-previous alignleft"><?php next_posts_link( __('Trang trước', 'twtheme') ); ?></div>
    <div class="nav-next alignright"><?php previous_posts_link( __('Trang sau', 'twtheme') ); ?></div>
<?php else : ?>
    <h3><?php _e('Không có bài viết nào được tìm thấy.'); ?></h3>
<?php endif; ?>

<?php get_footer(); ?>