<?php 
	$fieldss['phone'] = get_field('pn_phone_number','option');
	$fieldss['bases'] = get_field('pn_add_base','option');
	$fieldss['email'] = get_field('pn_email','option');
	$fieldss['facebook'] = get_field('pn_link_facebook','option');
	$fieldss['twitter'] = get_field('pn_link_twitter','option');
	$fieldss['google'] = get_field('pn_link_google','option');
	$fieldss['rss'] = get_field('pn_link_rss','option');
	$fieldss['youtube'] = get_field('pn_link_youtube','option');
	$fieldss['open'] = get_field('pn_open','option');
	$fieldss['close'] = get_field('pn_close','option');
	$fieldss['copyright'] = get_field('pn_copyright','option');
?>

			<div class="modal fade" id="view-product-popup" role="dialog">
				<div class="view-product-popup view-product-popup-wrap modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
							<div class="container-fluid">
								<div class="row">
									<div class="col-lg-5 col-md-6">
										<div class="image-zoom">
											<div class="owl-carousel owl-theme">
												<div class="item" data-hash="zero"><img src="<?php echo TFT_URL; ?>/public/images/ta_images-zoom1.jpg" alt=""/></div>
												<div class="item" data-hash="one"><img src="<?php echo TFT_URL; ?>/public/images/ta_images-zoom2.jpg" alt=""/></div>
												<div class="item" data-hash="two"><img src="<?php echo TFT_URL; ?>/public/images/ta_images-zoom3.jpg" alt=""/></div>
												<div class="item" data-hash="three"><img src="<?php echo TFT_URL; ?>/public/images/ta_images-zoom4.jpg" alt=""/></div>
												<div class="item" data-hash="four"><img src="<?php echo TFT_URL; ?>/public/images/ta_images-zoom5.jpg" alt=""/></div>
											</div>
										</div>
										<div class="nav-thuml">
											<div class="owl-carousel owl-theme">
												<div class="item"><a href="#zero"><img src="<?php echo TFT_URL; ?>/public/images/ta_thuml1.jpg" alt=""/></a></div>
												<div class="item"><a href="#one"><img src="<?php echo TFT_URL; ?>/public/images/ta_thuml2.jpg" alt=""/></a></div>
												<div class="item"><a href="#two"><img src="<?php echo TFT_URL; ?>/public/images/ta_thuml3.jpg" alt=""/></a></div>
												<div class="item"><a href="#three"><img src="<?php echo TFT_URL; ?>/public/images/ta_thuml4.jpg" alt=""/></a></div>
												<div class="item"><a href="#four"><img src="<?php echo TFT_URL; ?>/public/images/ta_thuml5.jpg" alt=""/></a></div>
											</div>
										</div>
									</div>
									<div class="col-lg-7 col-md-6">
										<div class="modal-title">
											<h4>Họp mặt 2 (dành cho 5 - 6 người)</h4>
										</div>
										<div class="modal-price">
											<h3> <span>1,160,000</span><sup> 
													<u>đ</u></sup></h3>
										</div>
										<form id="cart-number" action="">
											<div class="number-input">
												<label>Số lượng</label>
												<input type="number" min="1" value="1"/>
											</div>
											<div class="add-input">
												<button type="submit">
													 Thêm vào giỏ</button><span>hoặc </span><a href="#">Xem chi tiết</a>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--.view-product-popup-wrap-->
			</div>
			<footer id="footer">
				<div class="up up-active"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
				<div class="footer-top">
					<div class="container">
						<div class="row">
							<div class="col-md-3 col-sm-12">
								<article>
									<div class="title">
										<h3>Thời gian</h3>
									</div>
									<div class="time">
										<div class="fleft">
											<p>Sáng</p>
										</div>
										<div class="fright">
											<p><?php echo $fieldss['open'] ?></p>
										</div>
										<div class="clear-fix"></div>
									</div>
									<div class="time">
										<div class="fleft">
											<p>Chiều</p>
										</div>
										<div class="fright">
											<p><?php echo $fieldss['close'] ?></p>
										</div>
										<div class="clear-fix"></div>
									</div>
								</article>
							</div>
							<div class="col-md-3 col-sm-12">
								<article>
									<div class="title">
										<h3>Thông tin khác</h3>
									</div>
									<ul class="list-check">
										<li><a href="#">Về chúng tôi</a></li>
										<li><a href="#">Hệ thống nhà hàng</a></li>
										<li><a href="#">Hướng dẫn mua hàng</a></li>
										<li><a href="#">Phương thức thanh toán</a></li>
										<li><a href="#">Phương thức vận chuyển </a></li>
										<li><a href="#">Chính sách bảo mật</a></li>
									</ul>
								</article>
							</div>
							<div class="col-md-6 col-sm-12">
								<article>
									<div class="title">
										<h3>Liện hệ</h3>
									</div>
									<?php 
									$dem_cs =1;
									?>
									<ul class="list-check">
										<?php foreach ($fieldss['bases'] as $key => $value): ?>
											<li>
												<p><i class="fas fa-home"></i><span>Cơ Sở <?= $dem_cs?>: <?php echo $value['pn_location_base']; $dem_cs++; ?></span></p>
											</li>
										<?php endforeach ?>
										<li>
											<p><i class="fas fa-envelope"></i><span>Email: <?php echo $fieldss['email'] ?></span></p>
										</li>
										<li>
											<p> <i class="fas fa-phone"></i><span>Hotline: <?php echo $fieldss['phone'] ?></span></p>
										</li>
									</ul>
									<ul class="list-inline list-socials">
										<li class="facebook"><a href="<?php echo $fieldss['facebook'] ?>"><i class="fab fa-facebook-f"></i></a></li>
										<li class="twitter"><a href="<?php echo $fieldss['twitter'] ?>"><i class="fab fa-twitter"></i></a></li>
										<li class="google"><a href="<?php echo $fieldss['google'] ?>"><i class="fab fa-google-plus-g"></i></a></li>
										<li class="rss"><a href="<?php echo $fieldss['rss'] ?>"><i class="fas fa-rss"></i></a></li>
										<li class="youtube"><a href="<?php echo $fieldss['youtube'] ?>"><i class="fab fa-youtube"></i></a></li>
									</ul>
								</article>
							</div>
							<div class="clear-fix"></div>
						</div>
					</div>
				</div>
				<div class="footer-bottom">
					<div class="container">
						<div class="footer-bottom__left fleft">
							<p><?php echo $fieldss['copyright'] ?></a>.</p>
						</div>
						<div class="footer-bottom__right fright">
							<ul class="list-inline list-pay">
								<li><a href="#"><img src="<?php echo TFT_URL; ?>/public/images/payment-1.png" alt=""/></a></li>
								<li><a href="#"><img src="<?php echo TFT_URL; ?>/public/images/payment-2.png" alt=""/></a></li>
								<li><a href="#"><img src="<?php echo TFT_URL; ?>/public/images/payment-3.png" alt=""/></a></li>
								<li><a href="#"><img src="<?php echo TFT_URL; ?>/public/images/payment-4.png" alt=""/></a></li>
							</ul>
						</div>
						<div class="clear-fix"></div>
					</div>
				</div>
				<!--#footer-->
			</footer>
			<div id="call-fixed"><a href="tel:013456789"><img src="<?php echo TFT_URL; ?>/public/images/ta_call-us.png" alt=""/></a></div>
		</div>
		<!--#wrapper-->
		<!--JS-->
		<script src="<?php echo TFT_URL; ?>/public/libs/jquery/jquery.min.js"></script>
		<script src="<?php echo TFT_URL; ?>/public/libs/owlcarousel/owl.carousel.min.js"></script>
		<script src="<?php echo TFT_URL; ?>/public/libs/validate/additional-methods.min.js"></script>
		<script src="<?php echo TFT_URL; ?>/public/libs/validate/jquery.validate.min.js"></script>
		<script src="<?php echo TFT_URL; ?>/public/libs/flexslider/jquery.flexslider-min.js"></script>
		<script src="<?php echo TFT_URL; ?>/public/libs/elevatezoom/jquery.elevatezoom.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="<?php echo TFT_URL; ?>/public/js/script.js"></script>
		<div id="ajax" style="display: none;">
			<script type="text/javascript">
				$(window).on('load',function(event) {
					// event.preventDefault();
					$('.add_to_cart').click(function(event) {
						//console.log($(this).attr('the-id'));
						var id_product= $(this).attr('the-id');
						$.ajax({
							type: 'POST',
							data: {
								action: 'add-to-cart',
								productID: id_product,
								number: 1,
								nonce: WPAjax.nonce,
							},
							url: WPAjax.url,
							beforeSend: function() {

							},
							success: function(data) {
								alert("Thêm vào giỏ hàng thành công");
								load_cart();
							}
						});
					});
					$('#add-to-cart-popup').click(function(event) {
						//console.log($(this).attr('the-id'));
						var id_product= $(this).attr('the-id');
						$.ajax({
							type: 'POST',
							data: {
								action: 'add-to-cart',
								productID: id_product,
								number: 1,
								nonce: WPAjax.nonce,
							},
							url: WPAjax.url,
							beforeSend: function() {

							},
							success: function(data) {
								alert("Thêm vào giỏ hàng thành công");
								load_cart();
							}
						});
					});
					$(".view-popupx").click(function(event) {
						// console.log($(this).attr('the-id'));
						var the_id = $(this).attr('the-id');
						$.ajax({
							type: 'POST',
							data: {
								action: 'load-product',
								productID: the_id,
								nonce: WPAjax.nonce
							},
							url: WPAjax.url,
							beforeSend: function() {
								var xxx = '<div class="view-product-popup view-product-popup-wrap modal-dialog"><div style="width: 100%;text-align: center;margin: 200px 0px;"><div class="modal-content"><div class="modal-body"><i style="font-size: 100px;color:black" class="fa fa-refresh fa-spin fa-3x fa-fw" aria-hidden="true"></i></div></div></div></div>';
								$('#view-product-popup').html(xxx);
							},
							success: function(data) {
								$('#view-product-popup').html(data);
								// alert("Mở popup thành công");
								$(".image-zoom .owl-carousel").owlCarousel({
							        loop: !0,
							        animateOut: "fadeOut",
							        animateIn: "fadaIn",
							        margin: 10,
							        nav: !1,
							        dots: !1,
							        responsive: {
							            0: {
							                items: 1
							            },
							            600: {
							                items: 1
							            },
							            1e3: {
							                items: 1
							            }
							        }
							    });
							    $(".nav-thuml .owl-carousel").owlCarousel({
							        loop: !1,
							        margin: 10,
							        nav: !0,
							        dots: !1,
							        center: !0,
							        mouseDrag: !1,
							        navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
							        responsive: {
							            0: {
							                items: 4
							            },
							            600: {
							                items: 4
							            },
							            1e3: {
							                items: 4
							            }
							        }
							    });
							},
							error: function(data) {
								console.log(data);
							}
						});
					});
				});
			</script>
			<script type="text/javascript">
				function load_cart() {
					load_cart_again = false;
					jQuery(document).ready(function($) {
						$.ajax({
							type: 'POST',
							data: {
							    action: 'load-cat-again',
							    nonce: WPAjax.nonce,
							},
							url: WPAjax.url,
							beforeSend: function() {
								var xxx = '<i style="font-size: 100px;color:black" class="fa fa-refresh fa-spin fa-3x fa-fw" aria-hidden="true"></i>';
								$('.cart-hover-pc').html(xxx);
							},
							success: function(data) {
								$('.cart-hover-pc').html(data);
							    //console.log(data);
							    return;
							}
						});
					})
				}
			</script>
		</div>
		<?php wp_footer() ?>
	</body>
</html>