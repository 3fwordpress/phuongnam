<?php 
/**
 * Comment file, the file show a post
 * called by comments_template() function
 * @author 3F Wordpress Team 
 * @link http://3fgroup.vn
 */
?>

<?php comment_form(); ?>
<ol class="commentlist">
    <?php wp_list_comments(); ?>
</ol>
